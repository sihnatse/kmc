program monte_carlo
  use parameters
  use transition_rate
  use soft_cluster
  use arg_parse
  implicit none

  integer i,j,k,l,l0,l1,l2,m,m2,n,i0,j0,k0,i1,j1,k1,i2,j2,k2,i3,j3,k3,i4,j4,k4,i22,j22,k22, &
    event,n_event_eq,n_deep_frozen,jumper,dumped, &
    r_cut,r_cut_x,r_cut_y,r_cut_z,r_cut_y2,r_cut_hit, &
    i_calc,ia3(3),sa,clock,iostatus
  integer,parameter :: seed = 86456
  integer, allocatable :: charge(:,:,:),charge3(:,:), &
    ind(:,:),dist(:,:),tmp3(:,:),tmp4(:)
  logical, allocatable :: charge_frozen(:)
  logical :: is_soft_pair,flip=.true.,cross_y
  real(8) :: temperature,waiting_time,current,conductivity,hop_distance,e_tr, &
    nt,mobility,efield,conductivity2,conductivity3, &
    sigma,rate_tot_jumper,e,r1,r2,r3,r4,r(3),e_fermi,de
  real(8), allocatable :: rates(:),rate_tot(:),rate_n(:,:), &
    tmp(:),tmp2(:),e_tr_loc(:)
  real time

  version=7

  call PARSE_ARGUMENTS()

  a=1.d0 !lattice constant = unit of distance
  sigma=1.d0 !dos window = unit of energy
  k_x=1.d0
  k_y=1.d0
  k_z=1.d0
  n_event_eq=5000

  loc_length2=2.0/loc_length
  dx2=float(nx)*k_x
  dy2=float(ny)*k_y
  dz2=float(nz)*k_z

  allocate(energy(nx,ny,nz),charge(nx,ny,nz),rates(n_charge),rate_tot(n_charge), &
    pos(3,nx,ny,nz),charge3(3,n_charge), &
    charge_frozen(n_charge),dist(3,n_charge),sc_id(nx,ny,nz),e_tr_loc(ny),tmp4(ny))
  allocate(track(n_charge),sc(2000))
  do i=1,n_charge
     allocate(track(i)%event(3,ny))
     track(i)%event=0
  end do
  do i=1,2000
     allocate(sc(i)%event(3,10))
     sc(i)%event=0
     sc(i)%size=0
  end do

  CALL SYSTEM_CLOCK(COUNT=clock)
  clock=mod(clock,seed)
  call srand(clock) !seed
  time=secnds(0.0)

  if (verbose) then
    write(*,'(2x,"n_charge=",i5)') n_charge
    write(*,'(2x,"n_event=",SE8.2)') float(n_event)
    write(*,'(2x,"n_event_eq=",i9)') n_event_eq
    write(*,'(2x,"loc_length=",f8.4," [a]")') loc_length
    if (pos_disorder) write(*,'(2x,"positional disorder")')
  end if
  open(1,file=output_file,status='old',access='append') !,status='replace'
  open(2,file='conv.dat',status='replace')
!  write(1,'(2x,"temperature",2x,"       bias",2x,"conductivty",2x,"hopdistance", &
!            2x," etransport",2x,"rcuthit",2x,"waitingtime",2x,"dumpd",2x,"    events")')
  open(3,file='tmp.dat',status='replace') !
  open(5,file='etr-output.dat',status='old',access='append')
!  open(6,file='occ-output.dat',status='old',access='append')

  e=0.d0 !seed for Metropolis-Hastings algorithm
  do i=1,nx
    do j=1,ny
      do k=1,nz
        select case (dos)
        case ('uniform')
          if (verbose.and.i==1.and.j==1.and.k==1) write(*,'(2x,"uniform DOS, W=[-1/2,+1/2]")')
          energy(i,j,k)=rand()-0.5
        case ('gaussian')
          !gaussian DOS, Metropolis-Hastings algorithm
          if (verbose.and.i==1.and.j==1.and.k==1) write(*,'(2x,"gaussian DOS, sigma=1")')
          do
            r1=4.d0*pi*sigma*(rand()-0.5)
            if (rand()<dexp(-(r1**2-e**2)/(2.d0*sigma**2))/dsqrt(2.d0*pi*sigma**2)) then
              e=r1
              energy(i,j,k)=e
              exit
            end if
          end do
        case ('exponential')
          !Metropolis-Hastings algorithm
          if (verbose.and.i==1.and.j==1.and.k==1) write(*,'(2x,"exponential DOS, sigma=1")')
          do
            r1=4.d0*pi*sigma*(rand()-0.5)
            r2=dexp(-1.d0*(dabs(r1)-dabs(e))/sigma) !exponential
            if (rand()<r2) then
              e=r1
              energy(i,j,k)=e
              exit
            end if
          end do
        end select

        pos(1,i,j,k)=float(i)*k_x+merge(0.8*(rand()-0.5)*k_x,0.d0,pos_disorder)
        pos(2,i,j,k)=float(j)*k_y+merge(0.8*(rand()-0.5)*k_y,0.d0,pos_disorder)
        pos(3,i,j,k)=float(k)*k_z+merge(0.8*(rand()-0.5)*k_z,0.d0,pos_disorder)
!        read(3,'(4(2x,e15.8))') energy(i,j,k),pos(1,i,j,k),pos(2,i,j,k),pos(3,i,j,k)
      end do
    end do
  end do
!  close(3)

  DO i_calc=1,calc_size
    temperature=calc(i_calc)%temperature
    r_cut=calc(i_calc)%r_cut
    bias=calc(i_calc)%bias

    coldness=1.d0/temperature
    bias2=bias/dble(ny)

    !cut-off distance to compute hops [in units of lattice constant]
    r_cut_x=merge(r_cut,0,nx>1); r_cut_y=r_cut; r_cut_z=merge(r_cut,0,nz>1)
    if (verbose) write(*,'(2x,"r_cut_x/y/z=",2(1x,i4,"/"),1x,i4)') r_cut_x,r_cut_y,r_cut_z
    r_cut_y2=r_cut_y**2
    l0=0
    do i0=-r_cut_x,+r_cut_x
      do j0=-r_cut_y,+r_cut_y
        do k0=-r_cut_z,+r_cut_z
          if (i0**2+j0**2+k0**2>r_cut_y2) cycle !sphere cut off
          if (i0==0.and.j0==0.and.k0==0) cycle
          l0=l0+1
        end do !k0
      end do !j0
    end do !i0

    allocate(ind(3,l0),rate_n(n_charge,l0))

    l=1
    do i0=-r_cut_x,+r_cut_x
      do j0=-r_cut_y,+r_cut_y
        do k0=-r_cut_z,+r_cut_z
          if (i0**2+j0**2+k0**2>r_cut_y2) cycle !sphere cut off
          if (i0==0.and.j0==0.and.k0==0) cycle
          ind(:,l)=(/i0,j0,k0/)
          l=l+1
        end do !k0
      end do !j0
    end do !i0

    charge=0;n=1 !initial energy distribution of charges
    do while (n<=n_charge)
      !T=infty solution
      i=nint(float(nx-1)*rand())+1
      j=nint(float(ny-1)*rand())+1
      k=nint(float(nz-1)*rand())+1
      if (charge(i,j,k)>0) cycle
      charge(i,j,k)=n
      charge3(:,n)=(/i,j,k/)
      n=n+1
    end do

    !Fermi energy
    de=(maxval(energy)-minval(energy))/1000.d0
    e_fermi=minval(energy)-6.d0*temperature
    r2=0.d0
    do while (r2<dble(n_charge)) !forward search for e_fermi
      e_fermi=e_fermi+de
      r2=0.d0
      do i=1,nx
        do j=1,ny
          do k=1,nz
            r2=r2+1.d0/(1.d0+exp((energy(i,j,k)-e_fermi)/temperature))
          end do !k0
        end do !j0
      end do !i0
!      r2=r2/(nx*ny*nz)
!      print *,e_fermi,r2,temperature
    end do
    print *,"e_fermi=",e_fermi
!    stop

    open(4,file='track.dat',status='replace'); close (4)
    do i=1,n_charge
      track(i)%event=0
    end do
    waiting_time=0.d0;rates=0.d0;rate_n=0.d0
    r_cut_hit=0;charge_frozen=.false.;sc_id=0
    n_deep_frozen=0;dumped=0;dist=0
    call SOFT_CLUSTER_INIT()
    DO event=1,n_event
      if (dumped==n_charge) exit

      do n=n_deep_frozen+1,n_charge
        if (charge_frozen(n)) then
          rates(n)=merge(rates(n-1),0.d0,n>1)+rate_n(n,l0)
          cycle
        end if
        i=charge3(1,n)
        j=charge3(2,n)
        k=charge3(3,n)

        is_soft_pair=(sc_id(i,j,k)>0)

        rate_tot(n)=0.d0 !total escape rate for charge n
        do m=1,l0
          i0=i+ind(1,m); i1=MD(i0,nx) !untrimed and trimmed coordinates where to hop
          j0=j+ind(2,m); j1=MD(j0,ny)
          k0=k+ind(3,m); k1=MD(k0,nz)
          if (charge(i1,j1,k1)==0) then
            if (is_soft_pair.and.sc_id(i,j,k)==sc_id(i1,j1,k1)) then
              r2=0.d0 !elimination
              !replace hopping rate inside soft cluster by sum of rates to leave
              !destination; if it'll be selected, decode then
              do m2=1,l0
                i3=i1+ind(1,m2); i4=MD(i3,nx)
                j3=j1+ind(2,m2); j4=MD(j3,ny)
                k3=k1+ind(3,m2); k4=MD(k3,nz)
                !hop to empty site and not within soft cluster sc_id(i,j,k)
                if (charge(i4,j4,k4)==0.and.sc_id(i4,j4,k4)/=sc_id(i,j,k)) then
                  r2=r2+RATE(i1,j1,k1,i3,j3,k3,i4,j4,k4)
                end if
              end do
              r4=RATE(i,j,k,i0,j0,k0,i1,j1,k1)
              r3=r4+merge(0.d0,rate_n(n,m-1),m==1) !escape rate from (i,j,k) including ->(i1,j1,k1)
              do m2=m+1,l0
                i3=i+ind(1,m2); i4=MD(i3,nx)
                j3=j+ind(2,m2); j4=MD(j3,ny)
                k3=k+ind(3,m2); k4=MD(k3,nz)
                if (charge(i4,j4,k4)==0.and.sc_id(i4,j4,k4)/=sc_id(i,j,k)) then
                  r3=r3+RATE(i,j,k,i3,j3,k3,i4,j4,k4)
                end if
              end do
              r3=r4/r3 !probability to hop in soft pair
              r2=r2*r3
              rate_tot(n)=rate_tot(n)+r4
            else
              r2=RATE(i,j,k,i0,j0,k0,i1,j1,k1)
              rate_tot(n)=rate_tot(n)+r2
            end if
            rate_n(n,m)=r2
          else
            rate_n(n,m)=0.d0 !already occupied site
          end if

          if (m>1) rate_n(n,m)=rate_n(n,m-1)+rate_n(n,m) !cumulative sum
        end do !m
        rates(n)=merge(rates(n-1),0.d0,n>1)+rate_n(n,l0)
      end do !n

      !Binary search: find who jumps.
10    e=rand()*rates(n_charge)
      if (e==0.d0) goto 10 !exclude random number generator's lower bound
      i1=1;i2=n_charge;jumper=i2/2
      do while (i2-i1>2)
        if (rates(jumper)<e) then
          i1=jumper
        else
          i2=jumper
        end if
        jumper=(i1+i2)/2
      end do
      if (e<rates(jumper-1)) jumper=jumper-1
      if (rates(jumper)<e) jumper=jumper+1
      if (jumper<1.or.n_charge<jumper) then
        !goto 10 - might be not enough, try hard
        n_deep_frozen=0;charge_frozen=.false.;cycle
      end if

      !Binary followed by linear search: find where it jumps.
      e=e-merge(rates(jumper-1),0.d0,1<jumper)
      i1=1;i2=l0;l=i2/2;
      do while (i2-i1>2)
        if (rate_n(jumper,l)<e) then
          i1=l
        else
          i2=l
        end if
        l=(i1+i2)/2
      end do
      do while (l>1.and.e<rate_n(jumper,l-1))
        l=l-1
      end do
      do while (l<l0.and.(rate_n(jumper,l)<e.or.rate_n(jumper,l)-rate_n(jumper,l-1)<1.d2*epsilon(e)))
        l=l+1
      end do

      rate_tot_jumper=rate_tot(jumper)
      i0=charge3(1,jumper); i1=i0+ind(1,l); i2=MD(i1,nx); i22=0
      j0=charge3(2,jumper); j1=j0+ind(2,l); j2=MD(j1,ny); j22=0
      k0=charge3(3,jumper); k1=k0+ind(3,l); k2=MD(k1,nz); k22=0
      dist(:,jumper)=dist(:,jumper)+ind(:,l)
      cross_y=(j1/=j2)

      if (sc_id(i0,j0,k0)>0.and.sc_id(i0,j0,k0)==sc_id(i2,j2,k2)) then
!          print *," cluster ",sc_id(i0,j0,k0),sc_id(i2,j2,k2)
!          print *,i0,j0,k0,i1,j1,k1,charge(i0,j0,k0),charge(i2,j2,k2)
        do m=1,l0
          i3=i2+ind(1,m); i4=MD(i3,nx)
          j3=j2+ind(2,m); j4=MD(j3,ny)
          k3=k2+ind(3,m); k4=MD(k3,nz)
          if (charge(i4,j4,k4)==0) then
            if (sc_id(i4,j4,k4)==sc_id(i2,j2,k2)) then
              r2=0.d0 !elimination
            else
              r2=RATE(i2,j2,k2,i3,j3,k3,i4,j4,k4)
            end if
            rate_n(jumper,m)=r2
          else
            rate_n(jumper,m)=0.d0
          end if
          if (m>1) rate_n(jumper,m)=rate_n(jumper,m-1)+rate_n(jumper,m) !cumulative sum
        end do !m
        rate_tot_jumper=rate_n(jumper,l0)
11      e=rand()*rate_n(jumper,l0)
        if (e==0.d0) goto 11
        l=1
        do while (rate_n(jumper,l)<e)
          l=l+1
        end do
        i1=i2+ind(1,l); i22=i2; i2=MD(i1,nx)
        j1=j2+ind(2,l); j22=j2; j2=MD(j1,ny)
        k1=k2+ind(3,l); k22=k2; k2=MD(k1,nz)
        dist(:,jumper)=dist(:,jumper)+ind(:,l)
        if (j1/=j2) cross_y=.true.
!          print *," cluster ",sc_id(i0,j0,k0),sc_id(i2,j2,k2)
!          print *,i0,j0,k0,i1,j1,k1,charge(i0,j0,k0),charge(i2,j2,k2)
      end if
      !check concentration integrity
      if (charge(i2,j2,k2)>0.or.charge(i0,j0,k0)==0) then
        write(*,*) "Error: charges get lost"
        write(*,*) rate_n(jumper,l-1),e,rate_n(jumper,l),l
        write(*,*) i0,j0,k0,i1,j1,k1,jumper,charge(i0,j0,k0),charge(i2,j2,k2)
        write(*,*) i22,j22,k22
        n_deep_frozen=0;charge_frozen=.false.;cycle !stop
      end if

      charge(i2,j2,k2)=charge(i0,j0,k0); charge(i0,j0,k0)=0
      charge3(:,jumper)=(/i2,j2,k2/)

      !mark charges to be recomputed
      n_deep_frozen=jumper-1;charge_frozen=.true.;charge_frozen(jumper)=.false.
      do m=1,l0
        !boost
        i3=i0+ind(1,m); i4=MD(i3,nx)
        j3=j0+ind(2,m); j4=MD(j3,ny)
        k3=k0+ind(3,m); k4=MD(k3,nz)
        n=charge(i4,j4,k4)
        if (n>0) then
          l=l0+1-m  !inverse coordinate (i4,j4,k4)->(i0,j0,k0)
          i3=MD2(i0-i4,r_cut_x,nx)
          j3=MD2(j0-j4,r_cut_y,ny)
          k3=MD2(k0-k4,r_cut_z,nz)
          r1=RATE(i4,j4,k4,i4+i3,j4+j3,k4+k3,i0,j0,k0)
          rate_n(n,l:)=rate_n(n,l:l0)+r1
          rate_tot(n)=rate_tot(n)+r1
          if (n-1<n_deep_frozen) n_deep_frozen=n-1 !to update rates()
        end if
        !kill
        i3=i2+ind(1,m); i4=MD(i3,nx)
        j3=j2+ind(2,m); j4=MD(j3,ny)
        k3=k2+ind(3,m); k4=MD(k3,nz)
        n=charge(i4,j4,k4)
        if (n>0) then
          l=l0+1-m
          r1=merge(rate_n(n,1),rate_n(n,l)-rate_n(n,l-1),l==1)
          rate_n(n,l:)=rate_n(n,l:l0)-r1
          rate_tot(n)=rate_tot(n)-r1
          if (n-1<n_deep_frozen) n_deep_frozen=n-1 !to update rates()
        end if
      end do

      if (event<n_event_eq) cycle
      if (event==n_event_eq) dist=0

      r1=-log(1.d0-rand())/rate_tot_jumper
      if (isnan(r1).or.(r1*0).ne.0) cycle
      waiting_time=waiting_time+r1

      !Increment the counter of the longest hop so that in the end we know how many
      !charges has been hopped by the cut radius. The larger this number the more likely
      !the cut radius should be increased not to restrict hopping.
      if (i22==0.and.j22==0.and.k22==0) then
        i=i1-i0; j=j1-j0; k=k1-k0
        if (dble(i**2+j**2+k**2)>(dble(r_cut_y)-0.99d0)**2) r_cut_hit=r_cut_hit+1
      end if

      !track hops
      if (cross_y) then !start tracking from boundary
        m=1; sa=size(track(jumper)%event,dim=2)
        do while (m<=sa.and..not.track(jumper)%event(2,m)==0)
          !start from slice 0 and finish in positive direction
          if (track(jumper)%event(2,1)<ny/2.and.ny/2<j0) then
            call DUMP_PATH(jumper,.true.,dumped)
            exit
          end if
          m=m+1
        end do
        track(jumper)%event=0 !reset
        track(jumper)%event(:,1)=(/i2,j2,k2/)
      else if (track(jumper)%event(2,1)>0) then
        if (i22==0.and.j22==0.and.k22==0) then
          i=i2;  j=j2;  k=k2
        else
          i=i22; j=j22; k=k22
        end if
        m=1; sa=size(track(jumper)%event,dim=2)
        do while (m<=sa.and..not.(track(jumper)%event(2,m)==0.or.all(track(jumper)%event(:,m)==(/i,j,k/))))
          m=m+1
        end do
        if (m+1>sa) then
          allocate(tmp3(3,sa))
          tmp3=track(jumper)%event
          deallocate(track(jumper)%event)
          allocate(track(jumper)%event(3,2*sa)) !doubling
          track(jumper)%event=0
          track(jumper)%event(:,1:sa)=tmp3
          deallocate (tmp3)
        end if
        track(jumper)%event(:,m:)=0
        track(jumper)%event(:,m)=(/i,j,k/)
        if (.not.(i22==0.and.j22==0.and.k22==0)) then
          track(jumper)%event(:,m+1)=(/i2,j2,k2/)
        end if
      end if

      if (eliminate_soft_clusters) then
        call SOFT_CLUSTER_FIND(i0,j0,k0,i2,j2,k2)
      end if

!        write(2,'(6(1x,i3),2x,e13.5,2x,i6,2x,e13.5,2x,e10.4,4(2x,i4))') i0,j0,k0,i1,j1,k1, &
!          waiting_time,sum(dist(2,:)),current,energy(i0,j0,k0)/qe,sc_unique_id, &
!          sc_id(i0,j0,k0),sc_id(i2,j2,k2),sc_id(i22,j22,k22)
!      if (n_event>10000.and.mod(event,n_event/10000)==0) &
!        write(2,'(2x,i10,2x,e13.6,2x,e11.5)') event,waiting_time, &
!        sum(dist(2,:))/(waiting_time*float(ny))
      END DO !event

    !dump all pieces if path count is insufficient
    if (dumped/=n_charge) then
      do i=1,n_charge
        call DUMP_PATH(i,.false.,dumped)
      end do
    end if
!    call DUMP_SOFT_CLUSTER()

    current=sum(dist(2,:))/(waiting_time*float(ny)) !Exactly the same as (forward - backward) in cross-section clasically
    conductivity=abs(current*dble(ny)/(bias*dble(nx*nz)*a)) !qe*
    e_tr=0.d0;e_tr_loc=0.d0;tmp4=0;hop_distance=0.d0
    open(4,file='track.dat',status='old',action='read')
    read (4,'(3(2x,i4))',IOSTAT=iostatus) i3,j3,k3
    if (iostatus==0) then
      j=0
      do
        read (4,'(3(2x,i4))',IOSTAT=iostatus) i4,j4,k4
        if (iostatus/=0) exit
        if (i3>0.and.j3>0.and.k3>0.and.i4>0.and.j4>0.and.k4>0) then
          if (abs(i4-i3)<=r_cut_x) then; dx=0; else if (i3>i4) then; dx=dx2; else; dx=-dx2; end if
          if (abs(k4-k3)<=r_cut_z) then; dz=0; else if (k3>k4) then; dz=dz2; else; dz=-dz2; end if
          r=pos(:,i4,j4,k4)-pos(:,i3,j3,k3)+(/dx,0.d0,dz/)
          hop_distance=hop_distance+dsqrt(r(1)**2+r(2)**2+r(3)**2)
          e_tr=e_tr+energy(i3,j3,k3)
          e_tr_loc(j3)=e_tr_loc(j3)+energy(i3,j3,k3)
          tmp4(j3)=tmp4(j3)+1
          j=j+1
        end if
        i3=i4; j3=j4; k3=k4
      end do
      e_tr=e_tr/dble(j)
      hop_distance=hop_distance/dble(j)
    end if
    write(5,'(2x,"temperature=",2x,e11.4)') temperature !save local (y) transport energies etr-output.dat
    do j=1,ny
      e_tr_loc(j)=e_tr_loc(j)/tmp4(j)
      write(5,'(2x,i4,2x,e11.4)') j,e_tr_loc(j)
    end do
    r1=0.d0;r2=0.d0;r3=0.d0
    do i=1,n_charge
      r1=r1+(dist(2,i)*a)**2
      r2=r2+dist(2,i)*a
      r3=r3+(dist(1,i)**2+dist(3,i)**2)*a**2
    end do
    conductivity2=(r1-r2**2)/(2.d0*waiting_time*nx*ny*nz*a**3*temperature) !qe**2*
    conductivity3=r3/(4.d0*waiting_time*nx*ny*nz*a**3*temperature) !qe**2*
    close (4)
    open(4,file='occ-output.dat',status='old',access='append')
    do i=1,nx
      do j=1,ny
        do k=1,nz
          if (myid<=2.and.charge(i,j,k)>0) write(2,'(2x,e15.8)') energy(i,j,k) !conv.dat
          if (myid<=2.and.charge(i,j,k)>0) write(4,'(2x,e15.8)') energy(i,j,k) !occ-output.dat
        end do
      end do
    end do
    close(4)
    nt=dble(n_charge)/(nx*ny*nz*a**3)
    efield=bias/(float(ny)*a)
    mobility=current/(qe*nt*efield*float(nx*nz)*a**2)

    if (verbose) write(*,'(2x,"temperatur",2x,"      bias",2x,"conductivi",2x,"  waittime",2x," rchit")')
    if (verbose) write(*,'(4(2x,e10.4),2x,i6)') temperature,bias,conductivity,waiting_time,r_cut_hit
    write(1,'(5(2x,e11.4),2x,i7,1(2x,e11.5),2x,i5,2x,i10,2x,e11.4)') &
      temperature,bias,conductivity,hop_distance,e_tr,r_cut_hit,waiting_time,dumped,event-1,e_fermi
    call flush()

    deallocate(ind,rate_n)
  END DO !i_calc

  deallocate(energy,charge,pos,rates,rate_tot,charge3,dist,e_tr_loc,tmp4)
  if (coulomb) then
    deallocate(e_coul)
  end if

  if (verbose) write(*,'(1x,"Total time = ",f15.4)') secnds(time)
  close(1); close(2); close(5)


end program monte_carlo
