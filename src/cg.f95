program monte_carlo
  use parameters
  use transition_rate
  use arg_parse
  use coulomb
  implicit none

  integer i,j,k,l,l0,l1,m,m2,n,i0,j0,k0,i1,j1,k1,i2,j2,k2,i3,j3,k3,i4,j4,k4,i22,j22,k22, &
    event,n_event_eq,n_deep_frozen,jumper,dumped, &
    r_cut,r_cut_x,r_cut_y,r_cut_z,r_cut_y2,r_cut_hit,r_cut_c, &
    i_calc,j_calc,ia3(3),sa,clock,iostatus,bin(200),reason,info
  integer,parameter :: seed = 86456
  integer, allocatable :: charge(:,:,:),charge_gs(:,:,:),charge_per_thread(:,:,:,:), &
    ind(:,:),dist(:,:),tmp3(:,:)
  logical, allocatable :: charge_frozen(:),flip_per_thread(:)
  logical :: is_soft_pair,flip=.true.,cross_y,is_equilibrated=.false.
  real(8) :: temperature,waiting_time,current,conductivity,hop_distance,e_tr, &
    nt,mobility,efield,conductivity2,conductivity3, &
    sigma,rate_tot_jumper,e,r1,r2,r3,r4,r(3),specific_heat,e_internal,chi,M_order,correlation_length, &
    acc_prob,convergence_criteria
  real(8), allocatable :: rates(:),rate_tot(:),rate_n(:,:), &
    tmp(:),tmp2(:),tmp4(:,:,:),occupancy(:,:,:),e_internal_per_thread(:), &
    specific_heat_per_thread(:,:),e_internal_per_thread0(:,:),M_order_per_thread(:,:), &
    correlation_length_per_thread(:,:),e_error(:),e_error_global(:),e_coul_aux_per_thread(:,:,:,:)
  real time,time1
  character(len=30) fm

  version=7

  call PARSE_ARGUMENTS_CG()

  a=1.d0 !lattice constant = unit of distance
  sigma=1.d0 !dos window = unit of energy
  k_x=1.d0
  k_y=1.d0
  k_z=1.d0
  n_event_eq=40 !5000
  convergence_criteria=1.d-3

  loc_length2=2.0/loc_length
  dx2=float(nx)*k_x
  dy2=float(ny)*k_y
  dz2=float(nz)*k_z

  allocate(energy(nx,ny,nz),charge(nx,ny,nz),charge_gs(nx,ny,nz),rates(n_charge),rate_tot(n_charge), &
    pos(3,nx,ny,nz),tmp4(nx,ny,nz), & !
    charge_frozen(n_charge),dist(3,n_charge),sc_id(nx,ny,nz),occupancy(nx,ny,nz), &
    charge_per_thread(nx,ny,nz,calc_size),e_internal_per_thread(calc_size),flip_per_thread(calc_size), &
    e_coul_aux_per_thread(nx,ny,nz,calc_size))
  allocate(track(n_charge),sc(2000))
  do i=1,n_charge
     allocate(track(i)%event(3,ny))
     track(i)%event=0
  end do
  do i=1,2000
     allocate(sc(i)%event(3,10))
     sc(i)%event=0
     sc(i)%size=0
  end do
  bin=0

  CALL SYSTEM_CLOCK(COUNT=clock)
  clock=mod(clock,seed)
  call srand(clock) !seed
  time=secnds(0.0)

  if (verbose) then
    write(*,'(2x,"n_charge=",i5)') n_charge
    write(*,'(2x,"n_event=",SE8.2)') float(n_event)
    write(*,'(2x,"n_event_eq=",i9)') n_event_eq
    write(*,'(2x,"loc_length=",f8.4," [a]")') loc_length
    if (pos_disorder) write(*,'(2x,"positional disorder")')
  end if
  open(1,file=output_file,status='old',access='append') !,status='replace'
  open(2,file='conv.dat',status='replace')
!  write(1,'(2x,"temperature",2x,"       bias",2x,"conductivty",2x,"hopdistance", &
!            2x," etransport",2x,"rcuthit",2x,"waitingtime",2x,"dumpd",2x,"    events")')
!  open(3,file='tmp.dat',status='old') !replace

  e=0.d0 !seed for Metropolis-Hastings algorithm
  do i=1,nx
    do j=1,ny
      do k=1,nz
        select case (dos)
        case ('uniform')
          if (verbose.and.i==1.and.j==1.and.k==1) write(*,'(2x,"uniform DOS, W=[-1/2,+1/2]")')
          energy(i,j,k)=rand()-0.5
        case ('gaussian')
          !gaussian DOS, Metropolis-Hastings algorithm
          if (verbose.and.i==1.and.j==1.and.k==1) write(*,'(2x,"gaussian DOS, sigma=1")')
          do
            r1=4.d0*pi*sigma*(rand()-0.5)
            if (rand()<dexp(-(r1**2-e**2)/(2.d0*sigma**2))/dsqrt(2.d0*pi*sigma**2)) then
              e=r1
              energy(i,j,k)=e
              exit
            end if
          end do
        case ('exponential')
          !Metropolis-Hastings algorithm
          if (verbose.and.i==1.and.j==1.and.k==1) write(*,'(2x,"exponential DOS, sigma=1")')
          do
            r1=4.d0*pi*sigma*(rand()-0.5)
            r2=dexp(-1.d0*(dabs(r1)-dabs(e))/sigma) !exponential
            if (rand()<r2) then
              e=r1
              energy(i,j,k)=e
              exit
            end if
          end do
        end select

        pos(1,i,j,k)=float(i)*k_x+merge(0.8*(rand()-0.5)*k_x,0.d0,pos_disorder)
        pos(2,i,j,k)=float(j)*k_y+merge(0.8*(rand()-0.5)*k_y,0.d0,pos_disorder)
        pos(3,i,j,k)=float(k)*k_z+merge(0.8*(rand()-0.5)*k_z,0.d0,pos_disorder)
!        read(3,'(4(2x,e15.8))') energy(i,j,k),pos(1,i,j,k),pos(2,i,j,k),pos(3,i,j,k)
      end do
    end do
  end do
!  close(3)

  !To avoid strange band gap in 3D, Coulomb cut-off should extend over whole device size
  !to the left and to the right. Does it suggest square integration region? No. Direct Coulomb converges conditionally.
  r_cut_c=n_c_period*ny-1 !floor(dble(ny)/2.d0)+n_replica*ny !n_replica*ny-1 !ny-1 !ny/2-1

  allocate(e_coul(nx,ny,nz))
  call COULOMB_INIT(r_cut_c)

  r_cut=ny/2 !?
  charge_gs=0;n=1  !initial energy distribution of charges
  select case (initc)
  case ('infinity') !T=infty solution
    if (verbose) write(*,'(2x,"Infinity T solution generated")')
    do while (n<=n_charge)
      i=nint(float(nx-1)*rand())+1
      j=nint(float(ny-1)*rand())+1
      k=nint(float(nz-1)*rand())+1
      if (charge_gs(i,j,k)>0) cycle
      charge_gs(i,j,k)=n
      n=n+1
    end do
!No ground state attempted because it is NOT a solution at T infinity
!    call COMPUTE_GROUND_STATE(r_cut,nx,ny,nz,charge_gs)
  case ('wigner0') !Wigner crystal, T=0, solution, valid for half-filling
    if (verbose) write(*,'(2x,"Wigner crystal T=0 generated")')
    do i=1,nx
      do j=1,ny
        do k=1,nz
          if (mod(i+j+k,2)==0) then
            charge_gs(i,j,k)=n
            n=n+1
          end if
        end do
      end do
    end do
  end select
  charge=charge_gs

  IF (replica_exchange) THEN
    write(*,'(2x,"Parallel tempering method with ",i3," Coulomb periodic")') n_c_period
    temperature=calc(1)%temperature !in units of W? e^2/a
    r_cut=calc(1)%r_cut
    e_dis_strength=calc(1)%e_dis_strength
    energy=energy*e_dis_strength !scale energies?
    if (verbose) write(*,'(2x,"min/max energies = ",2(2x,e10.4))') minval(energy),maxval(energy)
    coldness=1.d0/temperature
    !cut-off distance to compute hops [in units of lattice constant]
    r_cut_x=merge(r_cut,0,nx>1); r_cut_y=r_cut; r_cut_z=merge(r_cut,0,nz>1)
    if (verbose) write(*,'(2x,"r_cut_x/y/z=  ",2(1x,i4,"/"),1x,i4)') r_cut_x,r_cut_y,r_cut_z

    allocate(e_error(calc_size),e_error_global(n_event), &
      specific_heat_per_thread(calc_size,n_event),e_internal_per_thread0(calc_size,n_event), &
      M_order_per_thread(calc_size,n_event),correlation_length_per_thread(calc_size,n_event))

    do i=1,calc_size
      charge_per_thread(:,:,:,i)=charge
    end do
    specific_heat_per_thread=0.d0;e_internal_per_thread0=0.d0;M_order_per_thread=0.d0;correlation_length_per_thread=0.d0
    !warm-up
    if (verbose) write (*,'(a25)',advance='no') "Warm-up"
    time1=secnds(0.0)
    do j_calc=1,calc_size
      if (1<j_calc) charge_per_thread(:,:,:,j_calc)=charge_per_thread(:,:,:,j_calc-1) !progagate resulting charges
      call COMPUTE_REPLICA(calc(j_calc)%temperature,10,r_cut,charge_per_thread(:,:,:,j_calc), &
         occupancy,e_internal_per_thread(j_calc),specific_heat,M_order,correlation_length,e_error(j_calc), &
         .true.,e_coul_aux_per_thread(:,:,:,j_calc),info)
      if (verbose) write (*,'(a1)',advance='no') '.'
    end do
    if (verbose) write(*,'(1x," done. Time = ",f8.2)') secnds(time1)
    event=1
    DO WHILE (event<n_event)
      time1=secnds(0.0)
      do j_calc=1,calc_size !all points at once
        !a swap should be attmpted every "correlation time" normal MC events
        call COMPUTE_REPLICA(calc(j_calc)%temperature,10,r_cut,charge_per_thread(:,:,:,j_calc),occupancy, &
                             e_internal_per_thread(j_calc),specific_heat,M_order,correlation_length,e_error(j_calc), &
                             .false.,e_coul_aux_per_thread(:,:,:,j_calc),info)
        specific_heat_per_thread(j_calc,event)=specific_heat
        e_internal_per_thread0(j_calc,event)=e_internal_per_thread(j_calc)
        M_order_per_thread(j_calc,event)=M_order
        correlation_length_per_thread(j_calc,event)=correlation_length
      end do

      !convergence check
      e_error_global(event)=sum(e_internal_per_thread(:)) !all energies, over all threads, need to be minimized
      i0=nint(event/9.0); if (i0==0) i0=1 !ranges on error estimation [Goethe_PRL]
      i1=nint(event/3.0); if (i1==0) i1=1
      r1=sum(e_error_global(i1:event))/(event-i1+1)
      r2=sum(e_error_global(i0:i1))/(i1-i0+1)
      r3=(r1-r2)/(r1+r2)
      if (verbose) then
        write (fm,'(a7,i2,a20)') "(2x,i4,",calc_size+2,"(1x,e10.4),2x,f8.2)"
        write (*,fm) merge(event,0,is_equilibrated), & !'(2x,i4,10(1x,e10.4),2x,f15.4)'
          (e_internal_per_thread(i),i=1,calc_size),sum(e_internal_per_thread(:)),r3,secnds(time1)
      end if
      if (event>n_event_eq.and.dabs(r3)<convergence_criteria) exit !stop calculation is converged

      flip_per_thread=.false.
      do j_calc=1,calc_size !try to flip threads randomly
        i0=ceiling(rand()*calc_size)
        j0=ceiling(rand()*calc_size)
        if (i0==j0.or.flip_per_thread(i0).or.flip_per_thread(j0)) then
          cycle !
        else if (i0<j0) then
          i=i0;j=j0
        else
          i=j0;j=i0
        end if
        if (e_internal_per_thread(i)-e_internal_per_thread(j)>0) then
          !difference in energies of the current states of two simulations E_high - E_low (total energies)
          acc_prob=exp(-n_charge*(e_internal_per_thread(i)-e_internal_per_thread(j))* &
                 (1.d0/calc(j)%temperature-1.d0/calc(i)%temperature))
        else
          acc_prob=1.d0
        end if
20      r4=rand()
        if (r4==0.d0) goto 20 !exclude random number generator's lower bound
        !Effectively, swaps should be performed between replicas below and above the glass transition
        !to overcome deep energy basin. But, practically, no T of the transition is known apriori,
        !at simulation time, so do swap randomly.
        if (r4<acc_prob) then !swap move
          write (*,'("swap:",2(2x,e12.4)," <->",2(2x,e12.4)," (",f4.2,"<",f4.2,")")') &
            calc(i)%temperature,e_internal_per_thread(i),calc(j)%temperature,e_internal_per_thread(j),r4,acc_prob
          charge=charge_per_thread(:,:,:,i)
          charge_per_thread(:,:,:,i)=charge_per_thread(:,:,:,j)
          charge_per_thread(:,:,:,j)=charge
          tmp4=e_coul_aux_per_thread(:,:,:,i)
          e_coul_aux_per_thread(:,:,:,i)=e_coul_aux_per_thread(:,:,:,j)
          e_coul_aux_per_thread(:,:,:,j)=tmp4
          flip_per_thread(i)=.true.
          flip_per_thread(j)=.true.
        end if
      end do !j_calc

      open(3,file='tmp.dat',status='replace')
        do j_calc=1,calc_size !output
          write(3,'(9(2x,e11.4))') calc(j_calc)%temperature,e_dis_strength, &
            e_internal_per_thread0(j_calc,event),specific_heat_per_thread(j_calc,event), &
            M_order_per_thread(j_calc,event),correlation_length_per_thread(j_calc,event)
        end do
      close(3)
      event=event+1
      if (.not.is_equilibrated.and.event==n_event_eq) then
        is_equilibrated=.true.
        event=1
      end if
    END DO !event

   open(3,file='tmp.dat',status='replace')
   do j_calc=1,calc_size !output
      if (event>n_event) event=n_event
      i0=nint(event/3.0)
      i1=event
      write(*,'(9(2x,e11.4))') calc(j_calc)%temperature,e_dis_strength, &
        sum(e_internal_per_thread0(j_calc,i0:i1))/(i1-i0+1), &
        sum(specific_heat_per_thread(j_calc,i0:i1))/(i1-i0+1), &
        sum(M_order_per_thread(j_calc,i0:i1))/(i1-i0+1), &
        sum(correlation_length_per_thread(j_calc,i0:i1))/(ny*(i1-i0+1))
      write(1,'(9(2x,e11.4))') calc(j_calc)%temperature,e_dis_strength, &
        sum(e_internal_per_thread0(j_calc,i0:i1))/(i1-i0+1), &
        sum(specific_heat_per_thread(j_calc,i0:i1))/(i1-i0+1), &
        sum(M_order_per_thread(j_calc,i0:i1))/(i1-i0+1), &
        sum(correlation_length_per_thread(j_calc,i0:i1))/(ny*(i1-i0+1))
      write (3,*) "temperature=",calc(j_calc)%temperature
      do i=1,nx
        do j=1,ny
          do k=1,nz
            write(3,*) i,j,k,charge_per_thread(i,j,k,j_calc),e_coul_aux_per_thread(i,j,k,j_calc)
          end do
        end do
      end do
    end do
    close(3)

    call flush()
    deallocate(e_error,e_error_global)

  ELSE !ordinary Metropolis Monte-Carlo algorithm
    DO i_calc=1,calc_size
      temperature=calc(i_calc)%temperature !in units of W? e^2/a
      r_cut=calc(i_calc)%r_cut

      e_dis_strength=calc(i_calc)%e_dis_strength
      energy=energy*e_dis_strength !scale energies
      if (verbose) write(*,'(2x,"min/max energies = ",2(2x,e10.4))') minval(energy),maxval(energy)

      coldness=1.d0/temperature

      !cut-off distance to compute hops [in units of lattice constant]
      r_cut_x=merge(r_cut,0,nx>1); r_cut_y=r_cut; r_cut_z=merge(r_cut,0,nz>1)
      if (verbose) write(*,'(2x,"r_cut_x/y/z=  ",2(1x,i4,"/"),1x,i4)') r_cut_x,r_cut_y,r_cut_z

!!    charge=charge_gs
      call COMPUTE_SPECIFIC_HEAT(r_cut,nx,ny,nz,charge,occupancy,e_internal,specific_heat,M_order,correlation_length,info)

      !if cooling down and info>0, stop further calculation because, solution is too bad (likely got stuck in local minimum)
      if (info>0.and.i_calc<calc_size.and.calc(i_calc+1)%temperature<temperature) EXIT

      !store accumulated statistics on DOS
      open(3,file='dos.dat',status='old',action='read')
      do i=1,200
        read(3,*,IOSTAT=reason) r1,bin(i)
        if (reason<0) then !end of file
          bin=0
          exit
        end if
      end do
      close(3)
      do i=1,nx
        do j=1,ny
          do k=1,nz
            do l=1,200
              if (-1.0+0.01*l<(energy(i,j,k)+e_coul(i,j,k)).and. &
                  (energy(i,j,k)+e_coul(i,j,k))<-1.0+0.01*(l+1)) bin(l)=bin(l)+1
            end do
          end do
        end do
      end do
      open(3,file='dos.dat',status='replace')
      do i=1,200
        write(3,*) -1.0+0.01*(i+0.5),bin(i)
      end do
      close(3)

      open(3,file='tmp.dat',status='replace')
      do i=1,nx
        do j=1,ny
          do k=1,nz
            write(3,*) i,j,k,charge(i,j,k),occupancy(i,j,k),energy(i,j,k)+e_coul(i,j,k)
          end do
        end do
      end do
      close(3)


    ! !finite size correlation length
    ! chi=0.d0;r1=0.d0;r2=0.d0;l1=0;M_order=0.d0
    ! do i=1,nx
    !   do j=1,ny
    !     do k=1,nz
    !       ! do i0=i-nx/2,i+nx/2
    !       !   do j0=j-ny/2,j+ny/2
    !       !     do k0=k-nz/2,k+nz/2
    !       !       if (i0==i.and.j0==j.and.k0==k) cycle
    !       !       !if ((i0-i)**2+(j0-j)**2+(k0-k)**2>r_cut_c_y**2) cycle !sphere cut off
    !       !       i1=MD(i0,nx)
    !       !       j1=MD(j0,ny)
    !       !       k1=MD(k0,nz)
    !       !       r1=r1*charge(i,j,k)*charge(i1,j1,k1)
    !       !       l1=l1+1
    !       !     end do !k0
    !       !   end do !j0
    !       ! end do !i0
    !       ! r2=r2+charge(i,j,k)
    !       M_order=M_order+(2.d0*charge(i,j,k)-1.d0)*(-1)**(i+j+k)
    !     end do !k
    !   end do !j
    ! end do !i
    ! !r1=r1/dble(l1)
    ! M_order=M_order/(nx*ny*nz)

      write(1,'(7(2x,e11.4))') temperature,e_dis_strength,e_internal,specific_heat,M_order,correlation_length

      call flush()
    END DO !i_calc
  END IF !method

  deallocate(energy,charge,charge_gs,pos,rates,rate_tot,dist,occupancy,charge_per_thread,e_internal_per_thread, &
    flip_per_thread,tmp4) !
  if (coulomb) then
    deallocate(e_coul)
  end if

  if (verbose) write(*,'(1x,"Total time = ",f15.4)') secnds(time)
  close(1); close(2)


end program monte_carlo
