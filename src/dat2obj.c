#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#define PATH_MAX 0

// https://stackoverflow.com/questions/36760771/how-to-calculate-a-circle-perpendicular-to-a-vector
static void make_basis(const float* Q, float* u, float* v) {
  float L = hypot(Q[0], hypot(Q[1], Q[2])); // length of Q
  float sigma = (Q[0] > 0.0f) ? L : -L; // copysign(l, Q[0]) if you have it
  float h = Q[0] + sigma; // first component of householder vector
  float beta = -1.0 / (sigma * h); // householder scale
  // apply to (0,1,0)'
  float f = beta * Q[1];
  u[0] = f * h;
  u[1] = 1.0f + f * Q[1];
  u[2] = f * Q[2];
  // apply to (0,0,1)'
  float g = beta * Q[2];
  v[0] = g * h;
  v[1] = g * Q[1];
  v[2] = 1.0f + g * Q[2];
}

float dot_product(float* v, float* u) {
  return (v[0] * u[0] + v[1] * u[1] + v[2] * u[2]);
}

void normalize(float* v) {
  float w = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
  v[0] /= w;
  v[1] /= w;
  v[2] /= w;
}

// https://stackoverflow.com/questions/7168484/3d-line-segment-and-plane-intersection
bool line_plane_intersection(float* ray, float* ray_origin, float* normal,
                             float* pplane, float* contact) {
  // get d value
  float d = dot_product(normal, pplane);

  if (dot_product(normal, ray) == 0) {
    return false; // no intersection, the line is parallel to the plane
  }

  // compute the x value for the directed line ray intersecting the plane
  float x = (d - dot_product(normal, ray_origin)) / dot_product(normal, ray);

  // output contact point
  float ray_normalized[3] = {ray[0], ray[1], ray[2]};
//    normalize(ray_normalized);
  contact[0] = ray_origin[0] + ray_normalized[0] * x;
  contact[1] = ray_origin[1] + ray_normalized[1] * x;
  contact[2] = ray_origin[2] + ray_normalized[2] * x;

  return (0.0f <= x && x <= 1.0f) ? true : false;
}


// https://stackoverflow.com/questions/40629345/fill-array-dynamicly-with-gradient-color-c
void rgb(double ratio, int* clr) {
    //we want to normalize ratio so that it fits in to 6 regions
    //where each region is 256 units long
    int normalized = (int)(ratio * 256 * 6);

    //find the distance to the start of the closest region
    int x = normalized % 256;

    int red = 0, grn = 0, blu = 0;
    switch(normalized / 256)
    {
    case 0: red = 255;     grn = x;       blu = 0;       break; //red
    case 1: red = 255 - x; grn = 255;     blu = 0;       break; //yellow
    case 2: red = 0;       grn = 255;     blu = x;       break; //green
    case 3: red = 0;       grn = 255 - x; blu = 255;     break; //cyan
    case 4: red = x;       grn = 0;       blu = 255;     break; //blue
    case 5: red = 255;     grn = 0;       blu = 255 - x; break; //magenta
    }

    clr[0] = red;
    clr[1] = grn;
    clr[2] = blu;
}

// https://stackoverflow.com/questions/46053009/visualize-rgb-gradient-going-from-red-to-blue-in-c
void rgb2(double value, int* rgb) {
  if (value > 1 || value < 0) return;
  if (value > 0.5) {
    value -= 0.5;
    rgb[0] = 0;
    rgb[1] = (int)((1 - 2 * value) * 255);
    rgb[2] = (int)(2 * value * 255);
  } else {
    rgb[0] = (int)((1 - 2 * value) *255);
    rgb[1] = (int)(2*value*255);
    rgb[2] = 0;
  }
}

int main() {
  int n = pow(2, 3);
  float radius = 0.25f, x, y, z, length = 10.0f;
  int nx = 50, ny = 50, nz = 50;

  int v3[3], v4[3] = {0, 0, 0}, v5[PATH_MAX][2][3], count[PATH_MAX], max_count = 1;
  for (int i = 0; i < PATH_MAX; i++) {
    v5[i][0][0] = v5[i][0][1] = v5[i][0][2] = v5[i][1][0] = v5[i][1][1] = v5[i][1][2] = 0;
    count[i] = 0;
  }
  int count_node[nx+1][ny+1][nz+1];
  for (int i = 0; i < nx + 1; i++) {
    for (int j = 0; j < ny + 1; j++) {
      for (int k = 0; k < nz + 1; k++) {
        count_node[i][j][k] = 0;
      }
    }
  }

  FILE *fp = fopen("track.dat", "r");
  while (fscanf(fp,"%6i %6i %6i", &v3[0], &v3[1], &v3[2]) != -1) {
    if (v3[0] == 0 && v3[1] == 0 && v3[2] == 0) {
      v4[0] = v4[1] = v4[2] = 0;
      continue;
    }
    if (v4[0] == 0 && v4[1] == 0 && v4[2] == 0) {
      v4[0] = v3[0];
      v4[1] = v3[1];
      v4[2] = v3[2];
      continue;
    }
    int i = 0;
    for (; i < PATH_MAX; i++) {
      if (count[i] == 0) { break; }
      if ((v5[i][0][0] == v4[0] && v5[i][0][1] == v4[1] && v5[i][0][2] == v4[2] &&
           v5[i][1][0] == v3[0] && v5[i][1][1] == v3[1] && v5[i][1][2] == v3[2]) ||
          (v5[i][0][0] == v3[0] && v5[i][0][1] == v3[1] && v5[i][0][2] == v3[2] &&
           v5[i][1][0] == v4[0] && v5[i][1][1] == v4[1] && v5[i][1][2] == v4[2])) {
        break;
      }
    }
    if (i == PATH_MAX) { printf("Too small array!\n"); return 1; }
    if (abs(v4[0] - v3[0]) < nx / 2 && abs(v4[1] - v3[1]) < nx / 2 && abs(v4[2] - v3[2]) < nx / 2) {
//    if (abs(v4[1] - v3[1]) < nx / 2 && !(abs(v4[0] - v3[0]) > nx / 2 && abs(v4[2] - v3[2]) > nx / 2)) {
      if (count[i] == 0) {
        v5[i][0][0] = v4[0];
        v5[i][0][1] = v4[1];
        v5[i][0][2] = v4[2];
        v5[i][1][0] = v3[0];
        v5[i][1][1] = v3[1];
        v5[i][1][2] = v3[2];
        count[i] = 1;
      } else {
        count[i]++;
        if (max_count < count[i]) {
          max_count = count[i];
        }
      }
    }
    v4[0] = v3[0];
    v4[1] = v3[1];
    v4[2] = v3[2];
  }
  fclose(fp);
  printf("max_count=%i\n", max_count);

max_count=53;
  FILE *fp1 = fopen("out.obj", "w");
  fprintf(fp1, "mtllib out.mtl\n\n");
  fprintf(fp1, "g current\n");
  FILE *fp2 = fopen("out.mtl", "w");
  int hold = 0, skip = 0, jn, jj;
  float contact[3];
  for (int j = 0; j < PATH_MAX && count[j] > 0; j++) {
    float v1[3], v2[3];

    // process intersection
    if (hold == 0 && skip == 0 && abs(v5[j][0][0] - v5[j][1][0]) > nx / 2) { // x
      // 1. intersect 2. substute by the first segment 3. store the second segment
//      printf("%6i %6i %6i %6i %6i %6i\n", v5[j][1][0], v5[j][1][1], v5[j][1][2], v5[j][0][0], v5[j][0][1], v5[j][0][2]);
      float ray[3] = {v5[j][0][0] + 50.0f * (v5[j][0][0] < v5[j][1][0] ? 1.0f : -1.0f) - v5[j][1][0], v5[j][0][1] - v5[j][1][1], v5[j][0][2] - v5[j][1][2]};
      // 4. if intersection in corner
      if (abs(v5[j][0][2] - v5[j][1][2]) > nx / 2) {
        ray[2] += (v5[j][0][2] < v5[j][1][2] ? 50.0f : -50.0f);
      }
      float ray_origin[3] = {v5[j][1][0], v5[j][1][1], v5[j][1][2]};
      float normal[3] = {1.0f, 0.0f, 0.0f};
      normalize(normal);
      float pplane[3] = {(v5[j][0][0] > v5[j][1][0]) ? 0.5f : 50.5f, 0.0f, 0.0f};
      bool ints = line_plane_intersection(ray, ray_origin, normal, pplane, contact);
      if (abs(v5[j][0][2] - v5[j][1][2]) > nx / 2 && contact[2] < 0.5f) {
        skip = 1;
        j--;
        continue;
      }
      v1[0] = v5[j][1][0];
      v1[1] = v5[j][1][1];
      v1[2] = v5[j][1][2];
      v2[0] = contact[0];
      v2[1] = contact[1];
      v2[2] = contact[2];
      contact[0] += (v5[j][0][0] < v5[j][1][0] ? -50.0f : 50.0f);
      if (abs(v5[j][0][2] - v5[j][1][2]) > nx / 2) {
        contact[2] += (v5[j][0][2] < v5[j][1][2] ? -50.0f : 50.0f);
      }
      hold = 1;
      if (count_node[v5[j][1][0]][v5[j][1][1]][v5[j][1][2]] < count[j]) {
        count_node[v5[j][1][0]][v5[j][1][1]][v5[j][1][2]] = count[j];
      }
    } else if (hold == 0 && abs(v5[j][0][2] - v5[j][1][2]) > nx / 2) { // z
      // 1. intersect 2. substute by the first segment 3. store the second segment
      float ray[3] = {v5[j][0][0]- v5[j][1][0], v5[j][0][1] - v5[j][1][1], v5[j][0][2] + (v5[j][0][2] < v5[j][1][2] ? 50.0f : -50.0f)  - v5[j][1][2]};
      // 4. if intersection in corner
      if (abs(v5[j][0][0] - v5[j][1][0]) > nx / 2) {
        ray[0] += 50.0f * (v5[j][0][0] < v5[j][1][0] ? 1.0f : -1.0f);
      }
      float ray_origin[3] = {v5[j][1][0], v5[j][1][1], v5[j][1][2]};
      float normal[3] = {0.0f, 0.0f, 1.0f};
      normalize(normal);
      float pplane[3] = {0.0f, 0.0f, (v5[j][0][2] > v5[j][1][2]) ? 0.5f : 50.5f};
      bool ints = line_plane_intersection(ray, ray_origin, normal, pplane, contact);
      v1[0] = v5[j][1][0];
      v1[1] = v5[j][1][1];
      v1[2] = v5[j][1][2];
      v2[0] = contact[0];
      v2[1] = contact[1];
      v2[2] = contact[2];
      contact[2] += (v5[j][0][2] < v5[j][1][2] ? -50.0f : 50.0f);
      if (abs(v5[j][0][0] - v5[j][1][0]) > nx / 2) {
        contact[0] += (v5[j][0][0] < v5[j][1][0] ? -50.0f : 50.0f);
      }
      skip = 0;
      hold = 1;
      if (count_node[v5[j][1][0]][v5[j][1][1]][v5[j][1][2]] < count[j]) {
        count_node[v5[j][1][0]][v5[j][1][1]][v5[j][1][2]] = count[j];
      }
    } else if (hold == 1) {
      // substitute by the second segment
      v1[0] = contact[0];
      v1[1] = contact[1];
      v1[2] = contact[2];
      v2[0] = v5[j-1][0][0];
      v2[1] = v5[j-1][0][1];
      v2[2] = v5[j-1][0][2];
      count[j] = count[j-1];
      hold = 0;
      for (int k = PATH_MAX; k > j; k--) { // shift
        v5[k][0][0] = v5[k-1][0][0];
        v5[k][0][1] = v5[k-1][0][1];
        v5[k][0][2] = v5[k-1][0][2];
        v5[k][1][0] = v5[k-1][1][0];
        v5[k][1][1] = v5[k-1][1][1];
        v5[k][1][2] = v5[k-1][1][2];
        count[k] = count[k-1];
      }
    } else {
      // no intersection
      v1[0] = (float)v5[j][0][0];
      v1[1] = (float)v5[j][0][1];
      v1[2] = (float)v5[j][0][2];
      v2[0] = (float)v5[j][1][0];
      v2[1] = (float)v5[j][1][1];
      v2[2] = (float)v5[j][1][2];
      if (count_node[v5[j][0][0]][v5[j][0][1]][v5[j][0][2]] < count[j]) {
        count_node[v5[j][0][0]][v5[j][0][1]][v5[j][0][2]] = count[j];
      }
      if (count_node[v5[j][1][0]][v5[j][1][1]][v5[j][1][2]] < count[j]) {
        count_node[v5[j][1][0]][v5[j][1][1]][v5[j][1][2]] = count[j];
      }
    }
//    float v1[3] = {(float)v5[j][0][0], (float)v5[j][0][1], (float)v5[j][0][2]};
//    float v2[3] = {(float)v5[j][1][0], (float)v5[j][1][1], (float)v5[j][1][2]};

    fprintf(fp1, "usemtl material%i\n", j);

    float Q[3] = {v2[0] - v1[0], v2[1] - v1[1], v2[2] - v1[2]};
    float u[3], v[3];
    make_basis(Q, u, v);

    float r = radius * sqrt((float)count[j] / (float)max_count);
    for (int i = 0; i < n; i++) {
      x = v1[0] + r * (cos(2.0f * M_PI * i / n) * u[0] + sin(2.0f * M_PI * i / n) * v[0]);
      y = v1[1] + r * (cos(2.0f * M_PI * i / n) * u[1] + sin(2.0f * M_PI * i / n) * v[1]);
      z = v1[2] + r * (cos(2.0f * M_PI * i / n) * u[2] + sin(2.0f * M_PI * i / n) * v[2]);
      fprintf(fp1, "v %f %f %f\n", x, y, z);
    }
    int cut1 = (v1[0] < 1.0f || 50.0f < v1[0] || v1[1] < 1.0f || 50.0f < v1[1] || v1[2] < 1.0f || 50.0f < v1[2]) ? 1 : 0;
    for (int i = 0; i < n; i++) {
      x = v2[0] + r * (cos(2.0f * M_PI * i / n) * u[0] + sin(2.0f * M_PI * i / n) * v[0]);
      y = v2[1] + r * (cos(2.0f * M_PI * i / n) * u[1] + sin(2.0f * M_PI * i / n) * v[1]);
      z = v2[2] + r * (cos(2.0f * M_PI * i / n) * u[2] + sin(2.0f * M_PI * i / n) * v[2]);
      fprintf(fp1, "v %f %f %f\n", x, y, z);
    }
    int cut2 = (v2[0] < 1.0f || 50.0f < v2[0] || v2[1] < 1.0f || 50.0f < v2[1] || v2[2] < 1.0f || 50.0f < v2[2]) ? 1 : 0;

    for (int i = 1; i <= n; i++) {
      fprintf(fp1, "f %i %i %i %i\n", i + (j * 2 * n), i % n + 1 + (j * 2 * n), i % n + n + 1 + (j * 2 * n), i + n + (j * 2 * n));
    }
    if (cut1 == 1) {
      fprintf(fp1, "f");
      for (int i = 1; i <= n; i++) {
        fprintf(fp1, " %i", i + (j * 2 * n));
      }
      fprintf(fp1, "\n");
    }
    if (cut2 == 1) {
      fprintf(fp1, "f");
      for (int i = 1; i <= n; i++) {
        fprintf(fp1, " %i", i % n + n + 1 + (j * 2 * n));
      }
      fprintf(fp1, "\n");
    }

    fprintf(fp2, "newmtl material%i\n", j);
//    float frac = (float)(count[j]) / (float)max_count;
    float frac = (float)(count[j] - 1) / (float)(max_count - 1);
    frac = (1.0f - frac);
    int clr[3];
    rgb2(frac, clr);
    fprintf(fp2, "Ka %8.4f %8.4f %8.4f\n", clr[0] / 255.0f, clr[1] / 255.0f, clr[2] / 255.0f);
    fprintf(fp2, "Kd %8.4f %8.4f %8.4f\n", clr[0] / 255.0f, clr[1] / 255.0f, clr[2] / 255.0f);
//    fprintf(fp2, "Ka %8.4f %8.4f %8.4f\n", 1.0f - frac, 1.0f - frac, 1.0f - frac);
//    fprintf(fp2, "Kd %8.4f %8.4f %8.4f\n", 1.0f - frac, 1.0f - frac, 1.0f - frac);
    fprintf(fp2, "Ks 1 1 1\n");
    fprintf(fp2, "Ns 500         # Very shiny.\n");
//    fprintf(fp2, "Tr %8.4f\n\n", 1.0f - frac);
    fprintf(fp2, "\n");
    jn = j * 2 * n + 2 * n;
    jj = j;
  }

  int jjj = 0;
  for (int i = 0; i < nx; i++) {
    for (int j = 0; j < nx; j++) {
      for (int k = 0; k < nx; k++) {
        if (count_node[i + 1][j + 1][k + 1] > 0) {
          fprintf(fp1, "usemtl material%i\n", jj);
          float r = radius * sqrt((float)count_node[i + 1][j + 1][k + 1] / (float)max_count);
          for (int h = 0; h <= n; h++) {
            for (int m = 0; m < n; m++) {
              x = 1* r * sin(M_PI * h / n) * cos(2.0f * M_PI * m / n) + i + 1;
              y = 1* r * sin(M_PI * h / n) * sin(2.0f * M_PI * m / n) + j + 1;
              z = 1* r * cos(M_PI * h / n) + k + 1;
              fprintf(fp1, "v %f %f %f\n", x, y, z);
            }
            if (h > 0) {
              for (int i = 1; i <= n; i++) {
                fprintf(fp1, "f %i %i %i %i\n", i + jn + (h-1)*n + jjj*(n*(n+1)), i % n + 1 + jn + (h-1)*n + jjj*(n*(n+1)), i % n + n + 1 + jn + (h-1)*n + jjj*(n*(n+1)), i + n + jn + (h-1)*n + jjj*(n*(n+1)));
              }
            }
          }
          fprintf(fp2, "newmtl material%i\n", jj);
//          float frac = (float)(count_node[i + 1][j + 1][k + 1]) / (float)max_count;
          float frac = (float)(count_node[i + 1][j + 1][k + 1] - 1) / (float)(max_count - 1);
          frac = (1.0f - frac);
          int clr[3];
          rgb2(frac, clr);
          fprintf(fp2, "Ka %8.4f %8.4f %8.4f\n", clr[0] / 255.0f, clr[1] / 255.0f, clr[2] / 255.0f);
          fprintf(fp2, "Kd %8.4f %8.4f %8.4f\n", clr[0] / 255.0f, clr[1] / 255.0f, clr[2] / 255.0f);
//          fprintf(fp2, "Ka %8.4f %8.4f %8.4f\n", 1.0f - frac, 1.0f - frac, 1.0f - frac);
//          fprintf(fp2, "Kd %8.4f %8.4f %8.4f\n", 1.0f - frac, 1.0f - frac, 1.0f - frac);
          fprintf(fp2, "Ks 1 1 1\n");
          fprintf(fp2, "Ns 500         # Very shiny.\n");
        //    fprintf(fp2, "Tr %8.4f\n\n", 1.0f - frac);
          fprintf(fp2, "\n");
          jj++;
          jjj++;
        }
      }
    }
  }


  fclose(fp1);
  fclose(fp2);

  return 0;
}
