# KMC
Kinetic Monte-Carlo for charge transport. Rejection free version.

![currents](data/cover.png)

## Repository Structure
The repository is divided into modules and apps which put functionality into executables. Main app is `kmc`. `cg` computes Colomb glass ground state properties. Auxiliary `dat2obj` converts currents from internal data format into common `obj/mtl` pair.

## System Architecture
Fortran 95. No libraries at this stage.

## Build
This project uses CMAKE to build. To build through command line:  

        git clone git@bitbucket.org:sihnatse/kmc.git
        mkdir build
        cd build
        cmake ..
        make

## Run
Convenient way to run calculation is by script, where input parameters are specified. If multiple runs needed, which is typically the case because of random disorder, the script also specifies loops. Example is given in `start.bash`

Bias is applied along y-direction.

## Technical Specifications

### Input and output format
Input parameters are provided as the command line arguments.

Output is written into three text files:

* `output.dat` with main results: temperature, bias, conductivity, hopping distance, transport energy, number of longest hops, waiting time, number of charges passed through the system and number of MC events.

* `track.dat` with coordinates of charges passed through the system. Zeros separate completed paths between terminals.

* `conv.dat` with auxiliary information like energies of occupied sites

## References

1. A. F. Voter, *Introduction to the Kinetic Monte Carlo Method*, in Radiation Effects in Solids, edited by K. E. Sickafus and E. A. Kotomin (Springer, NATO Publishing Unit, Dordrecht, The Netherlands, 2005)

2. S. Ihnatsenka, "Interface effects on the thermoelectric properties of disordered semiconductors", Phys. Rev. B 110, 214207 (2024)

3. M. Goethe and M. Palassini, *Phase diagram, correlation gap, and critical properties of the Coulomb glass*, Phys. Rev. Lett. 103, 045702 (2009)

4. J. H. Davies, P. A. Lee and T. M. Rice, *Properties of the electron glass*, Phys. Rev. B 29, 4260 (1984)
