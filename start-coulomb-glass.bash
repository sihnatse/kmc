#!/bin/bash

COMMON_ARGS="\
--nx 50 --ny 50 --nz 50 \
--loc_length 1.0 \
--dos gaussian \
--events 100000000 --charges 1020 \
--verbose "
#--events 1000 --charges 32 \


COULOMB_ARGS="\
--replica-exchange \
--n_c_period 2 \
--ewald \
--ewald_cutoff 1.0 \
--initc infinity"
#--initc wigner0"

#--calc <temperature> <E disorder strength> <bias> <cut-off radius>
# parameters for Mott law test
CALCULATION_ARGS="\
--calc 0.2 1.0 6 "
#--calc 0.05 0.0 0.25 9 "
#--calc 1.0 0.0 5.0 6 \
#--calc 0.5 0.0 2.5 6 \
#--calc 0.2 0.0 1.0 7 \
#--calc 0.4 0.0 1.0 3 \
#--calc 0.3 0.0 1.0 3 \
#--calc 0.27 0.0 1.0 3 \
#--calc 0.25 0.0 1.0 3 \
#--calc 0.22 0.0 1.0 3 \
#--calc 0.2 0.0 1.0 3 \
#--calc 0.15 0.0 1.0 3 \
#--calc 0.1 0.0 1.0 3 "
#--calc 1.0 0.0 1.0 3 \
#--calc 0.8 0.0 1.0 3 \
#--calc 0.7 0.0 1.0 3 \
#--calc 0.6 0.0 1.0 3 \
#--calc 1.2 0.0 1.0 5 \
#--calc 1.5 0.0 1.0 5 "
#--calc 0.15 0.0 1.0 5 \
#--calc 0.2 0.0 1.0 5 \
#--calc 0.25 0.0 1.0 5 \
#--calc 0.3 0.0 1.0 5 \
#--calc 0.4 0.0 1.0 5 \
#--calc 0.5 0.0 1.0 5 \

# output file must exist before writing to it
if [ ! -f output.dat ]; then
  touch output.dat
fi

if [ ! -f dos.dat ]; then
  touch dos.dat
fi

# write header with names of computed parameters
if [ ! -s output.dat ]; then
  echo "  temperature  edisstrngth         bias   e_internal" \
       "      spheat      M_order   corrlength" >> output.dat
fi

for ((i = 0; i < 1; i++))
do
  ./build/cg \
    $COMMON_ARGS $COULOMB_ARGS $CALCULATION_ARGS
done


