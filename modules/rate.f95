module transition_rate
  use parameters
  implicit none
  private
  public RATE

contains

  !(i0,j0,k0) -> (i1,j1,k1)
  real(8) function RATE(i0,j0,k0,i1,j1,k1,i2,j2,k2)
    integer,intent(in) :: i0,j0,k0,i1,j1,k1,i2,j2,k2
    real(8) :: r1,r2,dx,dy,dz,r(3)

    dx=0.d0;dy=0.d0;dz=0.d0 !must be initialized like this
    if (i1>i2) then
      dx=dx2
    else if (i1<i2) then
      dx=-dx2
    end if
    if (j1>j2) then
      dy=dy2
    else if (j1<j2) then
      dy=-dy2
    end if
    if (k1>k2) then
      dz=dz2
    else if (k1<k2) then
      dz=-dz2
    end if
    r=pos(:,i2,j2,k2)-pos(:,i0,j0,k0)+(/dx,dy,dz/)
    r2=dsqrt(r(1)**2+r(2)**2+r(3)**2) !NORM2(r) !dsqrt(dot_product(r,r)) - slower
    r1=energy(i2,j2,k2)-energy(i0,j0,k0)-bias2*(r(2)+dy)

    if (r1>0.d0) then
      RATE=dexp(-r2*loc_length2-r1*coldness)
    else
      RATE=dexp(-r2*loc_length2)
    end if
  end function RATE
  
end module transition_rate
    
