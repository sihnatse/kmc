module arg_parse
  use parameters

  implicit none

contains

  subroutine PARSE_ARGUMENTS()
    integer :: n_args,i
    character(len=32) :: arg,value
    character(len=32), dimension(:), allocatable :: args
    type(point), dimension(:), allocatable :: tmp


    n_args=command_argument_count()
    if (n_args==0) then
        print *,'Error: no arguments provided'
        print *,'More info with: "kmc --help"'
        stop
    end if
    allocate(args(n_args))

    i=1
    do while (i<=n_args)
      call get_command_argument(i,args(i))
      arg=trim(args(i))
      if (arg(1:2)/='--') then
        print *,"Error: wrong parameter format ",arg
        stop
      end if
      if (arg=='--help'.or.arg=='--version') then
        write (*,'("KMC - Kinetic Monte-Carlo for charge transport v.",i1)') version
        if (arg=='--help') then
          write (*,*)
          write (*,'("usage: kmc [arguments] [--version] [--help]")')
          write (*,*)
          write (*,'("Arguments:")')
          write (*,'("   --nx <value>            Number of sites in x direction")')
          write (*,'("   --ny <value>            Number of sites in y direction")')
          write (*,'("   --nz <value>            Number of sites in z direction")')
          write (*,'("   --loc_length <value>    Localization length in units of lattice constant")')
          write (*,'("   --dos <value>           Density Of States [uniform, gaussian, exponential]")')
          write (*,'("   --pos-disorder          Apply positional disorder")')
          write (*,'("   --events <value>        Maximum number of Monte-Carlo events")')
          write (*,'("   --charges <value>       Number of charges")')
          write (*,'("   --soft-clusters         Eliminate soft clusters")')
          write (*,'("   --verbose               Be verbose")')
          write (*,'("   --output <file>         Save results to <file> (default is output.dat)")')
          write (*,'("   --calc <T> <W> <V> <rc> Calculate for temperature, energy disorder strength, bias, cut-off radius")')
        end if
        stop
      end if
      if (i+1<=n_args) then
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1)) !it might contain dummy
      end if
      select case (arg)
      case ('--nx')
        read (value,'(i32)') nx
        i=i+1
      case ('--ny')
        read (value,'(i32)') ny
        i=i+1
      case ('--nz')
        read (value,'(i32)') nz
        i=i+1
      case ('--loc_length')
        read (value,'(d32.8)') loc_length
        i=i+1
      case ('--events')
        read (value,'(i32)') n_event
        i=i+1
      case ('--charges')
        read (value,'(i32)') n_charge
        i=i+1
      case ('--dos')
        read (value,'(a32)') dos
        if (.not.(dos=='uniform'.or.dos=='gaussian'.or.dos=='exponential')) then
          print *,'Error: unknown dos value: "',dos,'"'
          stop
        end if
        i=i+1
      case ('--pos-disorder')
        pos_disorder=.true.
      case ('--soft-clusters')
        eliminate_soft_clusters=.true.
      case ('--verbose')
        verbose=.true.
      case ('--output')
        read (value,'(a32)') output_file
        output_file=trim(output_file)
        i=i+1
      case ('--calc')
        allocate(tmp(calc_size+1))
				if (calc_size>0) tmp(1:calc_size)=calc
				if (calc_size>0) deallocate(calc)
				call move_alloc(tmp,calc)
				calc_size=calc_size+1
        read (value,'(d32.8)') calc(calc_size)%temperature
        i=i+1
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1))

        read (value,'(d32.8)') calc(calc_size)%e_dis_strength
        i=i+1
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1))

        read (value,'(d32.8)') calc(calc_size)%bias
        i=i+1
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1))
        read (value,'(i32)') calc(calc_size)%r_cut
        i=i+1
      case default
        print *,'Error: unknown parameter: "',trim(arg),'"'
        stop
      end select
      i=i+1
    end do
  end subroutine PARSE_ARGUMENTS


  subroutine PARSE_ARGUMENTS_CG()
    integer :: n_args,i
    character(len=32) :: arg,value
    character(len=32), dimension(:), allocatable :: args
    type(point), dimension(:), allocatable :: tmp


    n_args=command_argument_count()
    if (n_args==0) then
      print *,'Error: no arguments provided'
      print *,'More info with: "cg --help"'
      stop
    end if
    allocate(args(n_args))

    i=1
    do while (i<=n_args)
      call get_command_argument(i,args(i))
      arg=trim(args(i))
      if (arg(1:2)/='--') then
        print *,"Error: wrong parameter format ",arg
        stop
      end if
      if (arg=='--help'.or.arg=='--version') then
        write (*,'("KMC - Kinetic Monte-Carlo for charge transport v.",i1)') version
        if (arg=='--help') then
          write (*,*)
          write (*,'("usage: cg [arguments] [--version] [--help]")')
          write (*,*)
          write (*,'("Arguments:")')
          write (*,'("   --nx <value>            Number of sites in x direction")')
          write (*,'("   --ny <value>            Number of sites in y direction")')
          write (*,'("   --nz <value>            Number of sites in z direction")')
          write (*,'("   --loc_length <value>    Localization length in units of lattice constant")')
          write (*,'("   --dos <value>           Density Of States [uniform, gaussian, exponential]")')
          write (*,'("   --pos-disorder          Apply positional disorder")')
          write (*,'("   --events <value>        Maximum number of Monte-Carlo events")')
          write (*,'("   --charges <value>       Number of charges")')
          write (*,'("   --verbose               Be verbose")')
          write (*,'("   --output <file>         Save results to <file> (default is output.dat)")')
          write (*,'("   --initc <value>         Initial charges [infinity, wigner0]")')
          write (*,'("   --replica-exchange      Use parallel tempering algorithm")')
          write (*,'("   --n_c_period <value>    Number of periodic domains for Coulomb")')
          write (*,'("   --ewald                 Use Ewald summation")')
          write (*,'("   --ewald_cutoff <value>  Cut-off in Ewald method in units of pi/L")')
          write (*,'("   --calc <T> <W> <rc>     Calculate for temperature, energy disorder strength, cut-off radius")')
        end if
        stop
      end if
      if (i+1<=n_args) then
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1)) !it might contain dummy
      end if
      select case (arg)
      case ('--nx')
        read (value,'(i32)') nx
        i=i+1
      case ('--ny')
        read (value,'(i32)') ny
        i=i+1
      case ('--nz')
        read (value,'(i32)') nz
        i=i+1
      case ('--loc_length')
        read (value,'(d32.8)') loc_length
        i=i+1
      case ('--events')
        read (value,'(i32)') n_event
        i=i+1
      case ('--charges')
        read (value,'(i32)') n_charge
        i=i+1
      case ('--dos')
        read (value,'(a32)') dos
        if (.not.(dos=='uniform'.or.dos=='gaussian'.or.dos=='exponential')) then
          print *,'Error: unknown dos value: "',dos,'"'
          stop
        end if
        i=i+1
      case ('--initc')
        read (value,'(a32)') initc
        if (.not.(initc=='infinity'.or.initc=='wigner0')) then
          print *,'Error: unknown initc value: "',initc,'"'
          stop
        end if
        i=i+1
      case ('--pos-disorder')
        pos_disorder=.true.
      case ('--verbose')
        verbose=.true.
      case ('--output')
        read (value,'(a32)') output_file
        output_file=trim(output_file)
        i=i+1
      case ('--calc')
        allocate(tmp(calc_size+1))
              if (calc_size>0) tmp(1:calc_size)=calc
              if (calc_size>0) deallocate(calc)
              call move_alloc(tmp,calc)
              calc_size=calc_size+1
        read (value,'(d32.8)') calc(calc_size)%temperature
        i=i+1
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1))

        read (value,'(d32.8)') calc(calc_size)%e_dis_strength
        i=i+1
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1))

        read (value,'(i32)') calc(calc_size)%r_cut
        i=i+1
      case ('--ground-state')
        ground_state=.true.
      case ('--replica-exchange')
        replica_exchange=.true.
      case ('--n_c_period')
        read (value,'(i32)') n_c_period
        i=i+1
      case ('--ewald')
        ewald=.true.
      case ('--ewald_cutoff')
        read (value,'(d32.8)') ewald_cutoff
        i=i+1
      case default
        print *,'Error: unknown parameter: "',trim(arg),'"'
        stop
      end select
      i=i+1
    end do

  end subroutine PARSE_ARGUMENTS_CG


  subroutine PARSE_ARGUMENTS_ACTIVE()
    integer :: n_args,i
    character(len=32) :: arg,value
    character(len=32), dimension(:), allocatable :: args
    type(point), dimension(:), allocatable :: tmp


    n_args=command_argument_count()
    if (n_args==0) then
      print *,'Error: no arguments provided'
      print *,'More info with: "active --help"'
      stop
    end if
    allocate(args(n_args))

    i=1
    do while (i<=n_args)
      call get_command_argument(i,args(i))
      arg=trim(args(i))
      if (arg(1:2)/='--') then
        print *,"Error: wrong parameter format ",arg
        stop
      end if
      if (arg=='--help'.or.arg=='--version') then
        write (*,'("KMC - Kinetic Monte-Carlo for charge transport v.",i1)') version
        if (arg=='--help') then
          write (*,*)
          write (*,'("usage: kmc [arguments] [--version] [--help]")')
          write (*,*)
          write (*,'("Arguments:")')
          write (*,'("   --nx <value>            Number of sites in x direction")')
          write (*,'("   --ny <value>            Number of sites in y direction")')
          write (*,'("   --nz <value>            Number of sites in z direction")')
          write (*,'("   --loc_length <value>    Localization length in units of lattice constant")')
          write (*,'("   --dos <value>           Density Of States [uniform, gaussian, exponential]")')
          write (*,'("   --pos-disorder          Apply positional disorder")')
          write (*,'("   --events <value>        Maximum number of Monte-Carlo events")')
          write (*,'("   --charges <value>       Number of charges")')
          write (*,'("   --speed <value>         Particle speed")')
          write (*,'("   --soft-clusters         Eliminate soft clusters")')
          write (*,'("   --verbose               Be verbose")')
          write (*,'("   --output <file>         Save results to <file> (default is output.dat)")')
          write (*,'("   --calc <T> <W> <V> <rc> Calculate for temperature, energy disorder strength, bias, cut-off radius")')
        end if
        stop
      end if
      if (i+1<=n_args) then
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1)) !it might contain dummy
      end if
      select case (arg)
      case ('--nx')
        read (value,'(i32)') nx
        i=i+1
      case ('--ny')
        read (value,'(i32)') ny
        i=i+1
      case ('--nz')
        read (value,'(i32)') nz
        i=i+1
      case ('--loc_length')
        read (value,'(d32.8)') loc_length
        i=i+1
      case ('--events')
        read (value,'(i32)') n_event
        i=i+1
      case ('--charges')
        read (value,'(i32)') n_charge
        i=i+1
      case ('--speed')
        read (value,'(d32.8)') speed_global
        i=i+1
      case ('--dos')
        read (value,'(a32)') dos
        if (.not.(dos=='uniform'.or.dos=='gaussian'.or.dos=='exponential')) then
          print *,'Error: unknown dos value: "',dos,'"'
          stop
        end if
        i=i+1
      case ('--pos-disorder')
        pos_disorder=.true.
      case ('--soft-clusters')
        eliminate_soft_clusters=.true.
      case ('--verbose')
        verbose=.true.
      case ('--output')
        read (value,'(a32)') output_file
        output_file=trim(output_file)
        i=i+1
      case ('--calc')
        allocate(tmp(calc_size+1))
              if (calc_size>0) tmp(1:calc_size)=calc
              if (calc_size>0) deallocate(calc)
              call move_alloc(tmp,calc)
              calc_size=calc_size+1
        read (value,'(d32.8)') calc(calc_size)%temperature
        i=i+1
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1))

        read (value,'(d32.8)') calc(calc_size)%e_dis_strength
        i=i+1
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1))

        read (value,'(d32.8)') calc(calc_size)%bias
        i=i+1
        call get_command_argument(i+1,args(i+1))
        value=trim(args(i+1))
        read (value,'(i32)') calc(calc_size)%r_cut
        i=i+1
      case default
        print *,'Error: unknown parameter: "',trim(arg),'"'
        stop
      end select
      i=i+1
    end do

  end subroutine PARSE_ARGUMENTS_ACTIVE

end module arg_parse
