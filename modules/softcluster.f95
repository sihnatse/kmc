module soft_cluster
  use parameters
  implicit none

  integer :: i_soft_pair,soft_pairs(6),sc_unique_id,soft_pair_max=10

  private
  public SOFT_CLUSTER_INIT, SOFT_CLUSTER_FIND, DUMP_SOFT_CLUSTER

contains

  subroutine SOFT_CLUSTER_INIT()
    sc_unique_id=0
    soft_pairs=0
  end subroutine SOFT_CLUSTER_INIT

  subroutine SOFT_CLUSTER_FIND(i0,j0,k0,i2,j2,k2)
    integer,intent(in) :: i0,j0,k0,i2,j2,k2
    integer :: i,j,k,l,l5,id0,id,sa5,m,sa
    integer, allocatable :: tmp3(:,:)

    if (all(soft_pairs==(/i2,j2,k2,i0,j0,k0/))) then
      i_soft_pair=i_soft_pair+1
    else
      i_soft_pair=0
    end if
    if (i_soft_pair==soft_pair_max) then !soft pair found
      if (sc_id(i0,j0,k0)>0.or.sc_id(i2,j2,k2)>0) then
        if (sc_id(i0,j0,k0)>0) then
          i=i2;j=j2;k=k2
          id0=sc_id(i0,j0,k0) !existing soft cluster
        else
          i=i0;j=j0;k=k0
          id0=sc_id(i2,j2,k2) !existing soft cluster
        end if
        l=sc(id0)%size+1; sa=size(sc(id0)%event,dim=2)
        if (sc_id(i0,j0,k0)>0.and.sc_id(i2,j2,k2)>0) then !merge two
          if (verbose) print *,"Soft cluster: merge two."
          id=sc_id(i,j,k)
          l5=sc(id)%size+1; sa5=size(sc(id)%event,dim=2)
          if (verbose) print *,"join clusters",id0,id,l-1,l5-1
          if (l+l5-1>sa) then
            if (verbose) print *,"reallocated"
            allocate(tmp3(3,2*(l+l5-1))) !doubling max
            tmp3=0; tmp3(:,1:sa)=sc(id0)%event
            deallocate(sc(id0)%event)
            call move_alloc(tmp3,sc(id0)%event)
          end if
          do m=1,l5-1
            sc(id0)%event(:,l)=sc(id)%event(:,m)
            sc(id0)%size=sc(id0)%size+1
            sc_id(sc(id)%event(1,m),sc(id)%event(2,m),sc(id)%event(3,m))=id0
            if (verbose) print *,l
            l=l+1
          end do
          sc(id)%event=0
          sc(id)%size=0
        else !expand soft cluster by one
          if (verbose) then
            print *,"Soft cluster: expand by one"
            do m=1,l-1
              write(*,'("     (",i3,2(1x,i3),")")') sc(id0)%event(1,m),sc(id0)%event(2,m),sc(id0)%event(3,m)
            end do
            write(*,'("     (",i3,2(1x,i3),")")') i,j,k !candidate
          end if
          if (l>sa) then
            if (verbose) print *,"reallocated"
            allocate(tmp3(3,2*sa)) !doubling
            tmp3=0; tmp3(:,1:sa)=sc(id0)%event
            deallocate(sc(id0)%event)
            call move_alloc(tmp3,sc(id0)%event)
          end if
          sc(id0)%event(:,l)=(/i,j,k/)
          sc(id0)%size=sc(id0)%size+1
          sc_id(i,j,k)=id0
        end if
        if (verbose) write (*,'("soft pair(",i3,")=(",i3,2(1x,i3),")-(",i3,2(1x,i3),")")') id0,i0,j0,k0,i2,j2,k2
      else !new soft pair
        sc_unique_id=sc_unique_id+1
        if (sc_unique_id>size(sc,dim=1)) then
          print *, "too many soft clusters!"
          stop
        end if
        sc(sc_unique_id)%event(:,1)=(/i0,j0,k0/)
        sc(sc_unique_id)%event(:,2)=(/i2,j2,k2/)
        sc(sc_unique_id)%size=2
        sc_id(i0,j0,k0)=sc_unique_id
        sc_id(i2,j2,k2)=sc_unique_id
        if (verbose) write (*,'("soft pair(",i3,")=(",i3,2(1x,i3),")-(",i3,2(1x,i3),")")') sc_unique_id,i0,j0,k0,i2,j2,k2
      end if

      i_soft_pair=0 !soft pair registered, reset count
    end if !i_soft_pair==soft_pair_max

    soft_pairs=(/i0,j0,k0,i2,j2,k2/)
  end subroutine SOFT_CLUSTER_FIND

  subroutine DUMP_SOFT_CLUSTER
    integer :: i,j
    open(3,file='softcluster.dat',status='replace')
    do i=1,sc_unique_id
      do j=1,sc(i)%size
        write (3,'(5(2x,i5))') i,j,sc(i)%event(1,j),sc(i)%event(2,j),sc(i)%event(3,j)
      end do
      write (3,'(5(2x,"    -"))')
    end do
    close(3)
  end subroutine DUMP_SOFT_CLUSTER

end module soft_cluster
