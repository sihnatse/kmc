module coulomb
!  use omp_lib
  use parameters, ONLY: dx2,dy2,dz2,pos,nx,ny,nz,MD,MD3,n_charge,energy,verbose,e_coul, &
    DISTANCE,DISTANCE3,UGAMMA,coldness,a,pi,Im1,ewald,ewald_cutoff
  implicit none

  integer :: r_cut_c_x,r_cut_c_y,r_cut_c_z,n_modes
  real(8) :: madelung,qa,k_coul,e_total,r_cut_c2,cutoff

  private
  public COULOMB_INIT,COMPUTE_GROUND_STATE,COMPUTE_SPECIFIC_HEAT,COMPUTE_REPLICA

contains
!dont forget qa c cutof
  subroutine COULOMB_INIT(r_cut_c)
    integer,intent(in) :: r_cut_c
    integer :: i,j,k,i0,j0,k0,i1,j1,k1,m,ss,tc,n,charge(nx,ny,nz)
    logical :: found=.false.

    r_cut_c_x=merge(r_cut_c,0,nx>1); r_cut_c_y=r_cut_c; r_cut_c_z=merge(r_cut_c,0,nz>1)
    k_coul=1.d0/2.d0 !Prefactor. Because occupation is 0 or 1 and filling factor is 0.5. Chemical potential = 0.
    !Not needed in Ewald.
    !units of energy = Coulomb term = e^2/(kappa*lattice_constant) = 1

    n_modes=2 !Number of modes in Fourier transform for Coulomb. Better at least 2, though quickly suppressed.
    cutoff=ewald_cutoff*pi/dble(ny) !2.d0*pi/dble(ny) !dsqrt(pi*(nx*ny*nz/2)**(1.d0/3.d0))/dble(ny) ! ![1/a] cubic 3D lattice is supposed
    r_cut_c2=(dsqrt(pi/((nx*ny*nz)/2)**.33)*ny)**2

    qa=0.d0;madelung=0.d0;tc=0 !max Coulomb repulsion
    i=merge(nx/2,1,nx>1); j=ny/2; k=merge(nz/2,1,nz>1) !center
    if (.not.ewald) then
      !To apply sphere cut off - find out sphere where the total charge contained is zero.
      do m=0,nint(ny/2.d0)*100
        r_cut_c2=(r_cut_c_y-dble(m)/100.0)**2
        tc=0
        do i0=-r_cut_c_x,r_cut_c_x
          do j0=-r_cut_c_y,r_cut_c_y
            do k0=-r_cut_c_z,r_cut_c_z
              if (i0==0.and.j0==0.and.k0==0) cycle
              if (i0**2+j0**2+k0**2>r_cut_c2) cycle !sphere cut off
              i1=MD3(i+i0,nx)
              j1=MD3(j+j0,ny)
              k1=MD3(k+k0,nz)
              tc=tc+(-1)**(i0+j0+k0)
            end do !k0
          end do !j0
        end do !i0
        if (abs(tc)<=2) then
          found=.true.
          print *,r_cut_c_y,dsqrt(r_cut_c2)
          exit
        end if
        r_cut_c2=(r_cut_c_y+dble(m)/100.0)**2
        tc=0
        do i0=-r_cut_c_x,r_cut_c_x
          do j0=-r_cut_c_y,r_cut_c_y
            do k0=-r_cut_c_z,r_cut_c_z
              if (i0==0.and.j0==0.and.k0==0) cycle
              if (i0**2+j0**2+k0**2>r_cut_c2) cycle !sphere cut off
              i1=MD3(i+i0,nx)
              j1=MD3(j+j0,ny)
              k1=MD3(k+k0,nz)
              tc=tc+(-1)**(i0+j0+k0)
            end do !k0
          end do !j0
        end do !i0
        if (abs(tc)<=2) then
          found=.true.
          print *,r_cut_c_y,dsqrt(r_cut_c2)
          exit
        end if
      end do !m
      if (.not.found) then
        print *,"No sphere cut-off of zero neutral charge"
        stop
      end if
    end if

    if (ewald) then
       madelung=COMPUTE_EWALD_MADELUNG()
    else
      tc=0;madelung=0.d0
      do i0=-r_cut_c_x,r_cut_c_x
        do j0=-r_cut_c_y,r_cut_c_y
          do k0=-r_cut_c_z,r_cut_c_z
            if (i0==0.and.j0==0.and.k0==0) cycle
            !To apply sphere cut off - find out spheres where the total charge contained is zero.
            if (i0**2+j0**2+k0**2>r_cut_c2) cycle !sphere cut off
            i1=MD3(i+i0,nx)
            j1=MD3(j+j0,ny)
            k1=MD3(k+k0,nz)
            qa=qa+k_coul/DISTANCE3(i,j,k,i+i0,j+j0,k+k0,i1,j1,k1)
            madelung=madelung+(-1)**(i0+j0+k0)/DISTANCE3(i,j,k,i+i0,j+j0,k+k0,i1,j1,k1)
            tc=tc+(-1)**(i0+j0+k0)
         end do !k0
        end do !j0
      end do !i0
      if (verbose) write(*,'(2x,"r_cut_c_x/y/z=",2(1x,i4,"/"),1x,i4)') r_cut_c_x,r_cut_c_y,r_cut_c_z
    end if
    if (verbose) print *,"Madelung constant = ",madelung,"/",tc !±1.747565 in 3D

  end subroutine COULOMB_INIT


  subroutine COMPUTE_GROUND_STATE(r_cut,nx,ny,nz,charge)
    integer,intent(in) :: r_cut,nx,ny,nz
    integer,intent(inout) :: charge(nx,ny,nz)
    integer :: i,j,k,i0,j0,k0,i1,j1,k1,i2,j2,k2,i3,j3,k3,i4,j4,k4, &
               n,charge_moved,m,l,l0,r_cut_x,r_cut_y,r_cut_z,r_cut_y2, &
               event,event_max
    real(8) :: r1,r2
    integer, allocatable :: charge_i(:),charge_j(:),charge_k(:),ind(:,:)
    real(8), allocatable :: e_coul_tmp(:,:,:)
    logical, allocatable :: charge_mask(:,:,:)

    event=1
    event_max=100

    r_cut_x=merge(r_cut,0,nx>1);r_cut_y=r_cut;r_cut_z=merge(r_cut,0,nz>1) !nz/2
    r_cut_y2=r_cut_y**2

    allocate(charge_i(n_charge),charge_j(n_charge),charge_k(n_charge),charge_mask(nx,ny,nz), &
      e_coul_tmp(nx,ny,nz))

    n=1;charge_mask=.false.
    do i=1,nx
      do j=1,ny
        do k=1,nz
          if (charge(i,j,k)>0) then
            charge_mask(i,j,k)=.true.
            charge_i(n)=i;charge_j(n)=j;charge_k(n)=k
            n=n+1
          end if
        end do
      end do
    end do

    l0=0
    do i0=-r_cut_x,+r_cut_x
      do j0=-r_cut_y,+r_cut_y
        do k0=-r_cut_z,+r_cut_z
          if (i0**2+j0**2+k0**2>r_cut_y2) cycle !sphere cut off
          if (i0==0.and.j0==0.and.k0==0) cycle
          l0=l0+1
        end do !k0
      end do !j0
    end do !i0

    allocate(ind(l0,3))

    l=1
    do i0=-r_cut_x,+r_cut_x
      do j0=-r_cut_y,+r_cut_y
        do k0=-r_cut_z,+r_cut_z
          if (i0**2+j0**2+k0**2>r_cut_y2) cycle !sphere cut off
          if (i0==0.and.j0==0.and.k0==0) cycle
          ind(l,1)=i0; ind(l,2)=j0; ind(l,3)=k0
          l=l+1
        end do !k0
      end do !j0
    end do !i0

    e_coul=0.d0
    do i=1,nx
      do j=1,ny
        do k=1,nz
          do i0=i-r_cut_c_x,i+r_cut_c_x
            do j0=j-r_cut_c_y,j+r_cut_c_y
              do k0=k-r_cut_c_z,k+r_cut_c_z
                if (i0==i.and.j0==j.and.k0==k) cycle
                if ((i0-i)**2+(j0-j)**2+(k0-k)**2>r_cut_c_y**2) cycle !sphere cut off
                i1=MD(i0,nx)
                j1=MD(j0,ny)
                k1=MD(k0,nz)
                if (charge(i1,j1,k1)>0) &
                  e_coul(i,j,k)=e_coul(i,j,k)+k_coul/DISTANCE(i,j,k,i0,j0,k0,i1,j1,k1)
              end do !k0
            end do !j0
          end do !i0
        end do !k
      end do !j
    end do !i
    e_coul=e_coul-qa*n_charge/dble(nx*ny*nz)
    e_total=sum(energy+e_coul/2.d0,charge_mask)
    if (verbose) write (*,'(2x,"Total energy = ",e16.8)') e_total

    DO
      charge_moved=0
      do n=1,n_charge
        i0=charge_i(n)
        j0=charge_j(n)
        k0=charge_k(n)
        do m=1,l0
          i1=i0+ind(m,1); i2=MD(i1,nx)
          j1=j0+ind(m,2); j2=MD(j1,ny)
          k1=k0+ind(m,3); k2=MD(k1,nz)
          if (charge(i2,j2,k2)==0) then !empty site

            r2=energy(i2,j2,k2)+e_coul(i2,j2,k2)-energy(i0,j0,k0)-e_coul(i0,j0,k0)- &
                 k_coul/DISTANCE(i0,j0,k0,i1,j1,k1,i2,j2,k2)
            if (r2>0.d0) cycle

            e_coul_tmp=e_coul+qa*n_charge/dble(nx*ny*nz)
            !boost
            do i3=i0-r_cut_c_x,i0+r_cut_c_x
              do j3=j0-r_cut_c_y,j0+r_cut_c_y
                do k3=k0-r_cut_c_z,k0+r_cut_c_z
                  if (i3==i0.and.j3==j0.and.k3==k0) cycle
                  if ((i3-i0)**2+(j3-j0)**2+(k3-k0)**2>r_cut_c_y**2) cycle !sphere cut off
                  i4=MD(i3,nx)
                  j4=MD(j3,ny)
                  k4=MD(k3,nz)
                  r1=k_coul/DISTANCE(i0,j0,k0,i3,j3,k3,i4,j4,k4)
                  e_coul_tmp(i4,j4,k4)=e_coul_tmp(i4,j4,k4)-r1
                end do !k3
              end do !j3
            end do !i3
            !kill
            do i3=i2-r_cut_c_x,i2+r_cut_c_x
              do j3=j2-r_cut_c_y,j2+r_cut_c_y
                do k3=k2-r_cut_c_z,k2+r_cut_c_z
                  if (i3==i2.and.j3==j2.and.k3==k2) cycle
                  if ((i3-i2)**2+(j3-j2)**2+(k3-k2)**2>r_cut_c_y**2) cycle !sphere cut off
                  i4=MD(i3,nx)
                  j4=MD(j3,ny)
                  k4=MD(k3,nz)
                  r1=k_coul/DISTANCE(i2,j2,k2,i3,j3,k3,i4,j4,k4)
                  e_coul_tmp(i4,j4,k4)=e_coul_tmp(i4,j4,k4)+r1
                end do !k3
              end do !j3
            end do !i3
            e_coul_tmp=e_coul_tmp-qa*n_charge/dble(nx*ny*nz)

            charge(i2,j2,k2)=charge(i0,j0,k0); charge(i0,j0,k0)=0
            charge_i(n)=i2
            charge_j(n)=j2
            charge_k(n)=k2
            charge_mask(i2,j2,k2)=.true.; charge_mask(i0,j0,k0)=.false.
            e_coul=e_coul_tmp
            e_total=sum(energy+e_coul_tmp/2.d0,charge_mask) !r2

            charge_moved=charge_moved+1
            exit !loop over m is terminated
          end if
        end do !m
      end do !n
      if (verbose) write (*,'(2x,i8,2x,i8,2x,e16.8)') event,charge_moved,e_total

      event=event+1
      if (charge_moved==0.or.event_max<event) exit
    END DO !charge_moved

    if (verbose) write (*,'(2x,"Total energy / N = ",e16.8)') e_total/n_charge

    deallocate(charge_i,charge_j,charge_k,charge_mask,e_coul_tmp,ind)
  end subroutine COMPUTE_GROUND_STATE

  ! try 100.000 events bin, and 10 bins, so total 1 million. Randomly chosen charge, to random place.
  !info=0 ok
  !info=1 error in energy is still too large
  subroutine COMPUTE_SPECIFIC_HEAT(r_cut,nx,ny,nz,charge,occupancy,e_internal,specific_heat,m_order, &
                                   correlation_length,info)
    integer,intent(in) :: r_cut,nx,ny,nz
    integer,intent(inout) :: charge(nx,ny,nz)
    integer,intent(out) :: info
    real(8),intent(out) :: occupancy(nx,ny,nz)
    real(8),intent(out) :: specific_heat,e_internal,m_order,correlation_length
    integer :: i,j,k,i0,j0,k0,i1,j1,k1,i2,j2,k2,i3,j3,k3,i4,j4,k4, &
               n,charge_moved,m,l,l0,l1,r_cut_x,r_cut_y,r_cut_z,r_cut_y2, &
               event,event_max,event_warmup,ifake,i_rate,charge_moved_per_event
    real(8) :: r1,r2,r3,acceptance_ratio,susceptibility,hop_distance_max,E_error
    logical :: iswarm
    complex(8) :: chi0,chi2pi,c1,c2
    integer, allocatable :: charge_i(:),charge_j(:),charge_k(:),ind(:,:)
    real(8), allocatable :: e_coul_tmp(:,:,:),e_total_per_event(:),rate_cumulative(:), &
                            rate_cumulative2(:,:),tmp1(:,:,:),tmp2(:,:,:), &
                            m_order_per_event(:),m_order_per_event_global(:), &
                            susceptibility_per_event(:,:),correlation_length_per_event(:), &
                            hop_distance(:)
    logical, allocatable :: charge_mask(:,:,:)
    real time

    if (verbose) write (*,'(2x,"Temperature = ",e16.8)') 1.d0/coldness

!!    call omp_set_num_threads(2)
!!    write(*,*) 'number of threads:',omp_get_max_threads()

    !total event no = event_warmup + event_max
    event_warmup=2 !first warmup
    event_max=100 !20then this quantity or if within error
    iswarm=.false.
    charge_moved_per_event=n_charge !10000 !Monte-Carlo steps per swap

    r_cut_x=merge(r_cut,0,nx>1);r_cut_y=r_cut;r_cut_z=merge(r_cut,0,nz>1) !nz/2
    r_cut_y2=r_cut_y**2

    allocate(charge_i(n_charge),charge_j(n_charge),charge_k(n_charge),charge_mask(nx,ny,nz), &
      e_coul_tmp(nx,ny,nz),e_total_per_event(event_max),m_order_per_event(event_max), &
      m_order_per_event_global(event_max), &
      tmp1(nx,ny,nz),tmp2(0:nx-1,0:ny-1,0:nz-1),susceptibility_per_event(3,event_max), &
      correlation_length_per_event(event_max),hop_distance(charge_moved_per_event))
    m_order_per_event=0.d0;m_order_per_event_global=0.d0;susceptibility_per_event=0.d0
    correlation_length_per_event=0.d0

    n=1;charge_mask=.false.
    do i=1,nx
      do j=1,ny
        do k=1,nz
          if (charge(i,j,k)>0) then
            charge_mask(i,j,k)=.true.
            charge_i(n)=i;charge_j(n)=j;charge_k(n)=k
            n=n+1
          end if
        end do
      end do
    end do

    l0=0
    do i0=-r_cut_x,+r_cut_x
      do j0=-r_cut_y,+r_cut_y
        do k0=-r_cut_z,+r_cut_z
          if (i0**2+j0**2+k0**2>r_cut_y2) cycle !sphere cut off
          if (i0==0.and.j0==0.and.k0==0) cycle
          l0=l0+1
        end do !k0
      end do !j0
    end do !i0

    allocate(ind(l0,3),rate_cumulative(0:n_charge*l0),rate_cumulative2(n_charge,l0))

    l=1
    do i0=-r_cut_x,+r_cut_x
      do j0=-r_cut_y,+r_cut_y
        do k0=-r_cut_z,+r_cut_z
          if (i0**2+j0**2+k0**2>r_cut_y2) cycle !sphere cut off
          if (i0==0.and.j0==0.and.k0==0) cycle
          ind(l,1)=i0; ind(l,2)=j0; ind(l,3)=k0
          l=l+1
        end do !k0
      end do !j0
    end do !i0

    e_coul=0.d0
!    !$omp parallel do private(j,k,i0,j0,k0,i1,j1,k1)
    do i=1,nx
!!      write(*,*) 'Thread',omp_get_thread_num(),':',i
      do j=1,ny
        do k=1,nz
          do i0=i-r_cut_c_x,i+r_cut_c_x
            do j0=j-r_cut_c_y,j+r_cut_c_y
              do k0=k-r_cut_c_z,k+r_cut_c_z
                if (i0==i.and.j0==j.and.k0==k) cycle !self-interaction from replica is still here
                if ((i0-i)**2+(j0-j)**2+(k0-k)**2>r_cut_c2) cycle !sphere cut off
                i1=MD3(i0,nx)
                j1=MD3(j0,ny)
                k1=MD3(k0,nz)
                if (charge(i1,j1,k1)>0) &
                  e_coul(i,j,k)=e_coul(i,j,k)+k_coul/DISTANCE3(i,j,k,i0,j0,k0,i1,j1,k1)
              end do !k0
            end do !j0
          end do !i0
        end do !k
      end do !j
    end do !i
!    !$omp end parallel do
    e_coul=e_coul-qa*n_charge/dble(nx*ny*nz) !qa - Coulomb regularization (max force)
    e_total=sum(energy+e_coul/2.d0,charge_mask)
    if (verbose) then
      write (*,'(2x,"Total energy / N = ",e16.8)') e_total/n_charge
      write (*, &
        '(5x,"event",3x,"#/event",12x,"energy",7x,"time",6x,"tot_rate",7x,"M_order",7x,"E_error")')
    end if

    event=1;tmp2=0.d0;occupancy=0.d0;E_error=1.d0
    do while ((event<=event_max.and.iswarm).or.(event<event_warmup.and..not.iswarm))
      time=secnds(0.0);hop_distance_max=0
      do charge_moved=1,charge_moved_per_event
        rate_cumulative2=0.d0 !rate_cumulative=0.d0;
        tmp1=energy+e_coul
        rate_cumulative=0.d0;l=1
!!      !$omp parallel do private(i0,j0,k0,m,i1,j1,k1,i2,j2,k2)
        do n=1,n_charge

!30 n=nint(float(n_charge-1)*rand())+1
!if (n<1.or.n>n_charge) print *,"Error: wrong change selected"

          i0=charge_i(n)
          j0=charge_j(n)
          k0=charge_k(n)

!do
!  m=nint(float(l0-1)*rand())+1
!  i1=i0+ind(m,1); i2=MD(i1,nx)
!  j1=j0+ind(m,2); j2=MD(j1,ny)
!  k1=k0+ind(m,3); k2=MD(k1,nz)
!  if (charge(i2,j2,k2)==0) exit
!end do
          do m=1,l0
            i1=i0+ind(m,1); i2=MD(i1,nx)
            j1=j0+ind(m,2); j2=MD(j1,ny)
            k1=k0+ind(m,3); k2=MD(k1,nz)

            if (charge(i2,j2,k2)==0) then
              r1=DISTANCE(i0,j0,k0,i1,j1,k1,i2,j2,k2)
              r2=tmp1(i2,j2,k2)-tmp1(i0,j0,k0)-k_coul/r1
!              acceptance_ratio=merge(1.d0,dexp(-r2*coldness),r2<0.d0)
              !"Because we are considering a tunneling process, we know that the dominant dependence
              !of transition rate on distance must be exponential" [Amb71]
              rate_cumulative(l)=rate_cumulative(l-1)+merge(1.d0,dexp(-r2*coldness),r2<0.d0)*dexp(-2.d0*r1)
            else
!              acceptance_ratio=0.d0
              rate_cumulative(l)=rate_cumulative(l-1)
            end if

  !!          if (m==1) then
  !!            rate_cumulative2(n,1)=acceptance_ratio
  !!          else
  !!            rate_cumulative2(n,m)=rate_cumulative2(n,m-1)+acceptance_ratio
  !!          end if
           l=l+1
          end do !m
        end do !n
  !!      !$omp end parallel do

  !!      rate_cumulative(1:l0)=rate_cumulative2(1,:)
  !!      do n=2,n_charge
  !!        i=(n-1)*l0
  !!        rate_cumulative(i+1:i+l0)=rate_cumulative(i)+rate_cumulative2(n,:)
  !!      end do

20      r1=rand()
        if (r1==0.d0) goto 20 !exclude random number generator's lower bound
  !if (r1>acceptance_ratio) goto 30 !cycle

        r1=r1*rate_cumulative(n_charge*l0)

        i=1;r2=rate_cumulative(1)
        do while (r2<r1.and.i<n_charge*l0)
          i=i+1
          r2=rate_cumulative(i)
        end do
        n=floor(dble(i)/dble(l0))+1 !this guy
        m=mod(dble(i),dble(l0)) !moves to
        if (m==0) then
          n=n-1
          m=l0
        end if

        i0=charge_i(n) !from
        j0=charge_j(n)
        k0=charge_k(n)
        i1=i0+ind(m,1); i2=MD(i1,nx) !to
        j1=j0+ind(m,2); j2=MD(j1,ny)
        k1=k0+ind(m,3); k2=MD(k1,nz)
        if (charge(i2,j2,k2)>1) print *, "Error!!!",n,m,l0
        hop_distance(charge_moved)=DISTANCE(i0,j0,k0,i1,j1,k1,i2,j2,k2)
        if (hop_distance_max<hop_distance(charge_moved)) hop_distance_max=hop_distance(charge_moved)

        e_coul_tmp=e_coul+qa*n_charge/dble(nx*ny*nz)
        !boost
        !$omp parallel do private(j3,k3,i4,j4,k4,r1)
        do i3=i0-r_cut_c_x,i0+r_cut_c_x
          do j3=j0-r_cut_c_y,j0+r_cut_c_y
            do k3=k0-r_cut_c_z,k0+r_cut_c_z
              if (i3==i0.and.j3==j0.and.k3==k0) cycle
              if ((i3-i0)**2+(j3-j0)**2+(k3-k0)**2>r_cut_c2) cycle !sphere cut off
              i4=MD3(i3,nx)
              j4=MD3(j3,ny)
              k4=MD3(k3,nz)
              r1=k_coul/DISTANCE3(i0,j0,k0,i3,j3,k3,i4,j4,k4)
              e_coul_tmp(i4,j4,k4)=e_coul_tmp(i4,j4,k4)-r1
            end do !k3
          end do !j3
        end do !i3
        !$omp end parallel do
        !kill
        !$omp parallel do private(j3,k3,i4,j4,k4,r1)
        do i3=i2-r_cut_c_x,i2+r_cut_c_x
          do j3=j2-r_cut_c_y,j2+r_cut_c_y
            do k3=k2-r_cut_c_z,k2+r_cut_c_z
              if (i3==i2.and.j3==j2.and.k3==k2) cycle
              if ((i3-i2)**2+(j3-j2)**2+(k3-k2)**2>r_cut_c2) cycle !sphere cut off
              i4=MD3(i3,nx)
              j4=MD3(j3,ny)
              k4=MD3(k3,nz)
              r1=k_coul/DISTANCE3(i2,j2,k2,i3,j3,k3,i4,j4,k4)
              e_coul_tmp(i4,j4,k4)=e_coul_tmp(i4,j4,k4)+r1
            end do !k3
          end do !j3
        end do !i3
        !$omp end parallel do
        e_coul_tmp=e_coul_tmp-qa*n_charge/dble(nx*ny*nz)

        charge(i2,j2,k2)=charge(i0,j0,k0); charge(i0,j0,k0)=0
        charge_i(n)=i2
        charge_j(n)=j2
        charge_k(n)=k2
        charge_mask(i2,j2,k2)=.true.; charge_mask(i0,j0,k0)=.false.
        e_coul=e_coul_tmp
        e_total=sum(energy+e_coul/2.d0,charge_mask)/n_charge
      end do !charge_moved

      chi0=0.d0;chi2pi=0.d0;c1=0.d0;c2=0.d0
      if (iswarm) then
        e_total_per_event(event)=e_total

        M_order=0.d0 !order parameter
        do i=1,nx
          do j=1,ny
            do k=1,nz
              M_order=M_order+merge(1.d0,-1.d0,charge(i,j,k)>0)*(-1)**(i+j+k)
              occupancy(i,j,k)=occupancy(i,j,k)+merge(1.d0,0.d0,charge(i,j,k)>0)
            end do !k
          end do !j
        end do !i
        m_order_per_event_global(event)=dabs(M_order)/(nx*ny*nz)

        !Because Coulomb tends to distribute charges into ordered domains between which 
        !still exist boundary, global symmetry is broken and the order parameter has more
        !sense if obtained as max of 1D-s.
        r1=0.d0;r2=0.d0
        do i=1,nx
          do k=1,nz
            call ORDER_CORRELATION_1D(ny,charge(i,:,k),mod(i+k,2),M_order,correlation_length)
            r1=r1+dabs(M_order)
            r2=r2+correlation_length
          end do !k
        end do !i
        m_order_per_event(event)=r1/(nx*ny*nz)
        correlation_length_per_event(event)=r2/(nx*ny*nz)
        if (nx>1) then
          r1=0.d0;r2=0.d0
          do j=1,ny
            do k=1,nz
              call ORDER_CORRELATION_1D(nx,charge(:,j,k),mod(j+k,2),M_order,correlation_length)
              r1=r1+dabs(M_order)
              r2=r2+correlation_length
            end do !k
          end do !j
          if (m_order_per_event(event)<r1/(nx*ny*nz)) then
            m_order_per_event(event)=r1/(nx*ny*nz)
            correlation_length_per_event(event)=r2/(nx*ny*nz)
          end if
        end if
        if (nz>1) then
          r1=0.d0;r2=0.d0
          do i=1,nx
            do j=1,ny
              call ORDER_CORRELATION_1D(nz,charge(i,j,:),mod(i+j,2),M_order,correlation_length)
              r1=r1+dabs(M_order)
              r2=r2+correlation_length
            end do !j
          end do !i
          if (m_order_per_event(event)<r1/(nx*ny*nz)) then
            m_order_per_event(event)=r1/(nx*ny*nz)
            correlation_length_per_event(event)=r2/(nx*ny*nz)
          end if
        end if

        !finite-size (charge order) correlation length, or two-point correlation function
        l1=0
        do i=1,nx
          do j=1,ny
            do k=1,nz
              do i0=i,i+nx-1
                do j0=j,j+ny-1
                  do k0=k,k+nz-1
                    if (i0==i.and.j0==j.and.k0==k) cycle
                    !if ((i0-i)**2+(j0-j)**2+(k0-k)**2>r_cut_c_y**2) cycle !sphere cut off
                    i1=MD(i0,nx)
                    j1=MD(j0,ny)
                    k1=MD(k0,nz)
                    c1=merge(1.d0,-1.d0,charge(i,j,k)>0)*(-1)**(i+j+k)*merge(1.d0,-1.d0,charge(i1,j1,k1)>0)*(-1)**(i1+j1+k1)
                    chi0=chi0+c1 !susceptibility
                    chi2pi=chi2pi+c1*cdexp(Im1*(2.d0*pi/((nx-1)*a))*(i0-i)) !fourier transform at momentum (2pi/L,0,0)
                    c2=c2+c1*cdexp(Im1*(2.d0*pi/((ny-1)*a))*(j0-j)) !y-direction
                    r1=merge(1.d0,-1.d0,charge(i,j,k)>0)*(-1)**(i+j+k)*merge(1.d0,-1.d0,charge(i1,j1,k1)>0)*(-1)**(i1+j1+k1)
                    tmp2(abs(i1-i),abs(j1-j),abs(k1-k))=tmp2(abs(i1-i),abs(j1-j),abs(k1-k))+ &
                                                        !charge(i,j,k)*charge(i1,j1,k1)
                                                        r1
                    l1=l1+1
                  end do !k0
                end do !j0
              end do !i0
            end do !k
          end do !j
        end do !i
        chi0=chi0/dble(l1)
        chi2pi=chi2pi/dble(l1)
        c2=c2/dble(l1)
        susceptibility_per_event(1,event)=dsqrt(dreal(chi0)**2+aimag(chi0)**2)
        susceptibility_per_event(2,event)=dsqrt(dreal(chi2pi)**2+aimag(chi2pi)**2)
        susceptibility_per_event(3,event)=dsqrt(dreal(c2)**2+aimag(c2)**2)
      end if

      i0=nint(event/9.0); if (i0==0) i0=1
      i1=nint(event/3.0); if (i1==0) i1=1
      E_error=sum(e_total_per_event(i1:event))*(i1-i0+1)/(sum(e_total_per_event(i0:i1))*(event-i1+1))-1.d0
      if (verbose) then
        r1=susceptibility_per_event(1,event)/susceptibility_per_event(2,event)
        r2=susceptibility_per_event(1,event)/susceptibility_per_event(3,event)
        write (*,'(2x,i8,2x,i8,2x,e16.8,2x,f9.2,3(2x,e12.4),4x,f5.2,"/",f5.2)') &
          merge(event,0,iswarm),charge_moved,e_total,secnds(time),rate_cumulative(n_charge*l0), &
          m_order_per_event_global(event),E_error, &
          sum(hop_distance)/float(charge_moved_per_event),hop_distance_max
   !       max(merge(dsqrt(r1-1.d0),dsqrt(1.d0-r1),r1>1.d0),merge(dsqrt(r2-1.d0),dsqrt(1.d0-r2),r2>1.d0))
   !     dsqrt(dreal(chi0)**2+aimag(chi0)**2),dsqrt(dreal(chi2pi)**2+aimag(chi2pi)**2), &
   !     dsqrt(dreal(c2)**2+aimag(c2)**2),m_order_per_event(event)
      end if

      !error is defined over interval 1/9..1/3, so at least 9 events after warm-up
      if (iswarm.and.event>9.and.dabs(E_error)<1.d-2) exit
      event=event+1
      if (.not.iswarm.and.event==event_warmup) then
        event=1 !reset
        iswarm=.true.
        e_total_per_event=0.d0
      end if
    end do !event

    if (event==event_max) then !exception
      !By some reason, charge configuration gets stuck in local minimum. However, global minumum is rather needed.
      !If cooling down, at this point, no reason to continue because at lower T it'll get even worse.
      info=1
    end if

    specific_heat=(sum(e_total_per_event*e_total_per_event)/dble(event)-(sum(e_total_per_event)/dble(event))**2)*coldness**2
    e_internal=sum(e_total_per_event)/dble(event) !MC time average aka thermal average
    M_order=sum(m_order_per_event_global)/dble(event)
    occupancy=occupancy/dble(event)
    tmp2=tmp2/dble(event)
    chi0=sum(tmp2)
    chi2pi=0.d0
    do i=0,nx-1
      do j=0,ny-1
        do k=0,nz-1
          chi2pi=chi2pi+tmp2(i,j,k)*cdexp(Im1*(2.d0*pi/(nx*a))*i)
        end do
      end do
    end do

    susceptibility=sum(susceptibility_per_event(1,:))/sum(susceptibility_per_event(2,:))
    correlation_length=merge(dsqrt(susceptibility-1.d0),dsqrt(1.d0-susceptibility),susceptibility>1.d0)
    r1=sum(susceptibility_per_event(1,:))/sum(susceptibility_per_event(3,:))
    r2=merge(dsqrt(r1-1.d0),dsqrt(1.d0-r1),r1>1.d0)
    if (r2>correlation_length) correlation_length=r2 !max(x,y) 

    if (verbose) write (*,'(2x,"Internal energy = ",e16.8)') e_internal
    if (verbose) write (*,'(2x,"Specific heat = ",e16.8)') specific_heat
    if (verbose) write (*,'(2x,"Order parameter = ",e16.8,"/",e16.8)') &
      sum(m_order_per_event)/dble(event),M_order
!    if (verbose) write (*,'(2x,"Susseptibility ratio = ",e16.8,"/",e16.8)') susceptibility, &
!      sum(susceptibility_per_event(1,:))/(sum(susceptibility_per_event(2:3,:))/2.d0)
!    print *,chi0,chi2pi
    if (verbose) write (*,'(2x,"Correlation length = ",e16.8)') correlation_length


    deallocate(charge_i,charge_j,charge_k,charge_mask,e_coul_tmp,ind,e_total_per_event, &
      m_order_per_event,rate_cumulative,rate_cumulative2,tmp1,tmp2,susceptibility_per_event, &
      correlation_length_per_event)
    info=0
  end subroutine COMPUTE_SPECIFIC_HEAT






  !Simulated tempering method. (replica exchange) Newman & Barkema §6.4
  subroutine COMPUTE_REPLICA(temperature,event_max,r_cut,charge,occupancy,e_internal,specific_heat,m_order, &
                            correlation_length,e_error,warmup,e_coul_aux,info)
    real(8),intent(in) :: temperature
    integer,intent(in) :: event_max,r_cut
    integer,intent(inout) :: charge(nx,ny,nz)
    integer,intent(out) :: info
    real(8),intent(out) :: occupancy(nx,ny,nz)
    real(8),intent(out) :: specific_heat,e_internal,m_order,correlation_length,e_error
    logical,intent(in) :: warmup
    real(8),intent(inout) :: e_coul_aux(nx,ny,nz)
    integer :: i,j,k,i0,j0,k0,i1,j1,k1,i2,j2,k2,i3,j3,k3,i4,j4,k4, &
               n,charge_moved,m,l,l0,l1,r_cut_x,r_cut_y,r_cut_z,r_cut_y2, &
               event,ifake,i_rate,charge_moved_per_event
    real(8) :: r1,r2,r3,acceptance_ratio,susceptibility,hop_distance_max
    complex(8) :: chi0,chi2pi,c1,c2
    integer, allocatable :: charge_i(:),charge_j(:),charge_k(:),ind(:,:)
    real(8), allocatable :: e_coul_tmp(:,:,:),e_total_per_event(:),rate_cumulative(:), &
                            rate_cumulative2(:,:),tmp1(:,:,:),tmp2(:,:,:), &
                            m_order_per_event(:),m_order_per_event_global(:), &
                            susceptibility_per_event(:,:),correlation_length_per_event(:), &
                            hop_distance(:)
    logical, allocatable :: charge_mask(:,:,:)
    real time

    charge_moved_per_event=n_charge !true Metropolis Monte-Carlo step

    r_cut_x=merge(r_cut,0,nx>1);r_cut_y=r_cut;r_cut_z=merge(r_cut,0,nz>1) !nz/2
    r_cut_y2=r_cut_y**2

    allocate(charge_i(n_charge),charge_j(n_charge),charge_k(n_charge),charge_mask(nx,ny,nz), &
      e_coul_tmp(nx,ny,nz),e_total_per_event(event_max),m_order_per_event(event_max), &
      m_order_per_event_global(event_max), &
      tmp1(nx,ny,nz),tmp2(0:nx-1,0:ny-1,0:nz-1),susceptibility_per_event(3,event_max), &
      correlation_length_per_event(event_max),hop_distance(charge_moved_per_event))
    m_order_per_event=0.d0;m_order_per_event_global=0.d0;susceptibility_per_event=0.d0
    correlation_length_per_event=0.d0

    n=1;charge_mask=.false.
    do i=1,nx
      do j=1,ny
        do k=1,nz
          if (charge(i,j,k)>0) then
            charge_mask(i,j,k)=.true.
            charge_i(n)=i;charge_j(n)=j;charge_k(n)=k
            n=n+1
          end if
        end do
      end do
    end do

    l0=0
    do i0=-r_cut_x,+r_cut_x
      do j0=-r_cut_y,+r_cut_y
        do k0=-r_cut_z,+r_cut_z
          if (i0**2+j0**2+k0**2>r_cut_y2) cycle !sphere cut off
          if (i0==0.and.j0==0.and.k0==0) cycle
          l0=l0+1
        end do !k0
      end do !j0
    end do !i0

    allocate(ind(l0,3),rate_cumulative(0:n_charge*l0),rate_cumulative2(n_charge,l0))

    l=1
    do i0=-r_cut_x,+r_cut_x
      do j0=-r_cut_y,+r_cut_y
        do k0=-r_cut_z,+r_cut_z
          if (i0**2+j0**2+k0**2>r_cut_y2) cycle !sphere cut off
          if (i0==0.and.j0==0.and.k0==0) cycle
          ind(l,1)=i0; ind(l,2)=j0; ind(l,3)=k0
          l=l+1
        end do !k0
      end do !j0
    end do !i0

    if (warmup) then
      if (ewald) then
        call COMPUTE_EWALD(nx,ny,nz,charge,e_coul)
      else
        e_coul=0.d0 ! Coulomb potential on all sites
        do i=1,nx
          do j=1,ny
            do k=1,nz
              do i0=i-r_cut_c_x,i+r_cut_c_x
                do j0=j-r_cut_c_y,j+r_cut_c_y
                  do k0=k-r_cut_c_z,k+r_cut_c_z
                    if (i0==i.and.j0==j.and.k0==k) cycle !self-interaction from replica is still here
                    if ((i0-i)**2+(j0-j)**2+(k0-k)**2>r_cut_c2) cycle !sphere cut off
                    i1=MD3(i0,nx)
                    j1=MD3(j0,ny)
                    k1=MD3(k0,nz)
                    if (charge(i1,j1,k1)>0) &
                      e_coul(i,j,k)=e_coul(i,j,k)+k_coul/DISTANCE3(i,j,k,i0,j0,k0,i1,j1,k1)
                  end do !k0
                end do !j0
              end do !i0
            end do !k
          end do !j
        end do !i
        e_coul=e_coul-qa*n_charge/dble(nx*ny*nz) !qa - Coulomb normalization (by max force)
      end if
    else
      e_coul=e_coul_aux
    end if
    !What does e_total mean? Why charge_mask?
    e_total=sum(energy+e_coul/2.d0,charge_mask) !1/2 is double summation of pairs

    event=1;tmp2=0.d0;occupancy=0.d0;E_error=1.d0
    do while (event<=event_max)
      time=secnds(0.0);hop_distance_max=0
      do charge_moved=1,charge_moved_per_event
        rate_cumulative2=0.d0 !rate_cumulative=0.d0;
        tmp1=energy+e_coul
        rate_cumulative=0.d0;l=1
        do n=1,n_charge
          i0=charge_i(n)
          j0=charge_j(n)
          k0=charge_k(n)
          do m=1,l0
            i1=i0+ind(m,1); i2=MD(i1,nx)
            j1=j0+ind(m,2); j2=MD(j1,ny)
            k1=k0+ind(m,3); k2=MD(k1,nz)

            if (charge(i2,j2,k2)==0) then
              r1=DISTANCE(i0,j0,k0,i1,j1,k1,i2,j2,k2)
              r2=tmp1(i2,j2,k2)-tmp1(i0,j0,k0)-k_coul/r1 !1.d0/r1 !
              !"Because we are considering a tunneling process, we know that the dominant dependence
              !of transition rate on distance must be exponential" [Amb71]
              rate_cumulative(l)=rate_cumulative(l-1)+merge(1.d0,dexp(-r2/temperature),r2<0.d0)*dexp(-2.d0*r1)
            else
              rate_cumulative(l)=rate_cumulative(l-1)
            end if

           l=l+1
          end do !m
        end do !n

20      r1=rand()
        if (r1==0.d0) goto 20 !exclude random number generator's lower bound

        r1=r1*rate_cumulative(n_charge*l0)

        i=1;r2=rate_cumulative(1)
        do while (r2<r1.and.i<n_charge*l0)
          i=i+1
          r2=rate_cumulative(i)
        end do
        n=floor(dble(i)/dble(l0))+1 !this guy
        m=mod(dble(i),dble(l0)) !moves to
        if (m==0) then
          n=n-1
          m=l0
        end if

        i0=charge_i(n) !from
        j0=charge_j(n)
        k0=charge_k(n)
        i1=i0+ind(m,1); i2=MD(i1,nx) !to
        j1=j0+ind(m,2); j2=MD(j1,ny)
        k1=k0+ind(m,3); k2=MD(k1,nz)
        if (charge(i2,j2,k2)>1) print *, "Error!!!",n,m,l0
        hop_distance(charge_moved)=DISTANCE(i0,j0,k0,i1,j1,k1,i2,j2,k2)
        if (hop_distance_max<hop_distance(charge_moved)) hop_distance_max=hop_distance(charge_moved)

        !update Coulomb energies only due to charge that has been moved
        if (ewald) then
          !boost
          do i3=i0-r_cut_c_x,i0+r_cut_c_x
            do j3=j0-r_cut_c_y,j0+r_cut_c_y
              do k3=k0-r_cut_c_z,k0+r_cut_c_z
                i4=MD3(i3,nx)
                j4=MD3(j3,ny)
                k4=MD3(k3,nz)
                e_coul(i4,j4,k4)=e_coul(i4,j4,k4)-COMPUTE_EWALD_PARTIAL(i0,j0,k0,i3,j3,k3,i4,j4,k4,charge(i0,j0,k0))
              end do !k3
            end do !j3
          end do !i3
          do i3=i2-r_cut_c_x,i2+r_cut_c_x
            do j3=j2-r_cut_c_y,j2+r_cut_c_y
              do k3=k2-r_cut_c_z,k2+r_cut_c_z
                i4=MD3(i3,nx)
                j4=MD3(j3,ny)
                k4=MD3(k3,nz)
                e_coul(i4,j4,k4)=e_coul(i4,j4,k4)-COMPUTE_EWALD_PARTIAL(i2,j2,k2,i3,j3,k3,i4,j4,k4,charge(i2,j2,k2))
              end do !k3
            end do !j3
          end do !i3
          e_coul(i2,j2,k2)=e_coul(i2,j2,k2)+merge(0.5d0,-0.5d0,charge(i2,j2,k2)>0)*cutoff
          e_coul(i0,j0,k0)=e_coul(i0,j0,k0)+merge(0.5d0,-0.5d0,charge(i0,j0,k0)>0)*cutoff
          charge(i2,j2,k2)=charge(i0,j0,k0); charge(i0,j0,k0)=0
          !kill
          do i3=i2-r_cut_c_x,i2+r_cut_c_x
            do j3=j2-r_cut_c_y,j2+r_cut_c_y
              do k3=k2-r_cut_c_z,k2+r_cut_c_z
                i4=MD3(i3,nx)
                j4=MD3(j3,ny)
                k4=MD3(k3,nz)
                e_coul(i4,j4,k4)=e_coul(i4,j4,k4)+COMPUTE_EWALD_PARTIAL(i2,j2,k2,i3,j3,k3,i4,j4,k4,charge(i2,j2,k2))
              end do !k3
            end do !j3
          end do !i3
          do i3=i0-r_cut_c_x,i0+r_cut_c_x
            do j3=j0-r_cut_c_y,j0+r_cut_c_y
              do k3=k0-r_cut_c_z,k0+r_cut_c_z
                i4=MD3(i3,nx)
                j4=MD3(j3,ny)
                k4=MD3(k3,nz)
                e_coul(i4,j4,k4)=e_coul(i4,j4,k4)+COMPUTE_EWALD_PARTIAL(i0,j0,k0,i3,j3,k3,i4,j4,k4,charge(i0,j0,k0))
              end do !k3
            end do !j3
          end do !i3
          e_coul(i2,j2,k2)=e_coul(i2,j2,k2)-merge(0.5d0,-0.5d0,charge(i2,j2,k2)>0)*cutoff
          e_coul(i0,j0,k0)=e_coul(i0,j0,k0)-merge(0.5d0,-0.5d0,charge(i0,j0,k0)>0)*cutoff
        else
          e_coul_tmp=e_coul+qa*n_charge/dble(nx*ny*nz)
          !boost
          do i3=i0-r_cut_c_x,i0+r_cut_c_x
            do j3=j0-r_cut_c_y,j0+r_cut_c_y
              do k3=k0-r_cut_c_z,k0+r_cut_c_z
                if (i3==i0.and.j3==j0.and.k3==k0) cycle
                if ((i3-i0)**2+(j3-j0)**2+(k3-k0)**2>r_cut_c2) cycle !sphere cut off
                i4=MD3(i3,nx)
                j4=MD3(j3,ny)
                k4=MD3(k3,nz)
                r1=k_coul/DISTANCE3(i0,j0,k0,i3,j3,k3,i4,j4,k4)
                e_coul_tmp(i4,j4,k4)=e_coul_tmp(i4,j4,k4)-r1
              end do !k3
            end do !j3
          end do !i3
          !kill
          do i3=i2-r_cut_c_x,i2+r_cut_c_x
            do j3=j2-r_cut_c_y,j2+r_cut_c_y
              do k3=k2-r_cut_c_z,k2+r_cut_c_z
                if (i3==i2.and.j3==j2.and.k3==k2) cycle
                if ((i3-i2)**2+(j3-j2)**2+(k3-k2)**2>r_cut_c2) cycle !sphere cut off
                i4=MD3(i3,nx)
                j4=MD3(j3,ny)
                k4=MD3(k3,nz)
                r1=k_coul/DISTANCE3(i2,j2,k2,i3,j3,k3,i4,j4,k4)
                e_coul_tmp(i4,j4,k4)=e_coul_tmp(i4,j4,k4)+r1
              end do !k3
            end do !j3
          end do !i3
          e_coul=e_coul_tmp-qa*n_charge/dble(nx*ny*nz)
          charge(i2,j2,k2)=charge(i0,j0,k0); charge(i0,j0,k0)=0
        end if

        charge_i(n)=i2
        charge_j(n)=j2
        charge_k(n)=k2
        charge_mask(i2,j2,k2)=.true.; charge_mask(i0,j0,k0)=.false.
        e_total=sum(energy+e_coul/2.d0,charge_mask)/n_charge
      end do !charge_moved

      chi0=0.d0;chi2pi=0.d0;c1=0.d0;c2=0.d0

      e_total_per_event(event)=e_total

      M_order=0.d0 !order parameter
      do i=1,nx
        do j=1,ny
          do k=1,nz
            M_order=M_order+merge(1.d0,-1.d0,charge(i,j,k)>0)*(-1)**(i+j+k)
            occupancy(i,j,k)=occupancy(i,j,k)+merge(1.d0,0.d0,charge(i,j,k)>0)
          end do !k
        end do !j
      end do !i
      m_order_per_event_global(event)=dabs(M_order)/(nx*ny*nz)

      !finite-size (charge order) correlation length, or two-point correlation function
      l1=0
      do i=1,nx
        do j=1,ny
          do k=1,nz
            do i0=i,i+nx-1
              do j0=j,j+ny-1
                do k0=k,k+nz-1
                  if (i0==i.and.j0==j.and.k0==k) cycle
                  !if ((i0-i)**2+(j0-j)**2+(k0-k)**2>r_cut_c_y**2) cycle !sphere cut off
                  i1=MD(i0,nx)
                  j1=MD(j0,ny)
                  k1=MD(k0,nz)
                  c1=merge(1.d0,-1.d0,charge(i,j,k)>0)*(-1)**(i+j+k)*merge(1.d0,-1.d0,charge(i1,j1,k1)>0)*(-1)**(i1+j1+k1)
                  chi0=chi0+c1 !susceptibility
                  chi2pi=chi2pi+c1*cdexp(Im1*(2.d0*pi/((nx-1)*a))*(i0-i)) !fourier transform at momentum (2pi/L,0,0)
                  c2=c2+c1*cdexp(Im1*(2.d0*pi/((ny-1)*a))*(j0-j)) !y-direction
                  r1=merge(1.d0,-1.d0,charge(i,j,k)>0)*(-1)**(i+j+k)*merge(1.d0,-1.d0,charge(i1,j1,k1)>0)*(-1)**(i1+j1+k1)
                  tmp2(abs(i1-i),abs(j1-j),abs(k1-k))=tmp2(abs(i1-i),abs(j1-j),abs(k1-k))+ &
                                                      !charge(i,j,k)*charge(i1,j1,k1)
                                                      r1
                  l1=l1+1
                end do !k0
              end do !j0
            end do !i0
          end do !k
        end do !j
      end do !i
      chi0=chi0/dble(l1)
      chi2pi=chi2pi/dble(l1)
      c2=c2/dble(l1)
      susceptibility_per_event(1,event)=dsqrt(dreal(chi0)**2+aimag(chi0)**2)
      susceptibility_per_event(2,event)=dsqrt(dreal(chi2pi)**2+aimag(chi2pi)**2)
      susceptibility_per_event(3,event)=dsqrt(dreal(c2)**2+aimag(c2)**2)


      i0=nint(event/9.0); if (i0==0) i0=1
      i1=nint(event/3.0); if (i1==0) i1=1
      E_error=sum(e_total_per_event(i1:event))*(i1-i0+1)/(sum(e_total_per_event(i0:i1))*(event-i1+1))-1.d0
!      if (verbose) then
!        r1=susceptibility_per_event(1,event)/susceptibility_per_event(2,event)
!        r2=susceptibility_per_event(1,event)/susceptibility_per_event(3,event)
!        write (*,'(2x,e12.4,2x,i8,2x,e16.8,2(2x,e12.4))') &
!          temperature,event,e_total,E_error,dabs(M_order)/(nx*ny*nz) !& !secnds(time),
!      end if

    !error is defined over interval 1/9..1/3, so at least 9 events after warm-up
!    if (iswarm.and.event>9.and.dabs(E_error)<1.d-2) exit
      event=event+1
    end do !event

    e_coul_aux=e_coul

    specific_heat=(sum(e_total_per_event*e_total_per_event)/dble(event_max)- &
                  (sum(e_total_per_event)/dble(event_max))**2)/temperature**2
    e_internal=sum(e_total_per_event)/dble(event_max) !MC time average aka thermal average
    M_order=sum(m_order_per_event_global)/dble(event_max)
    occupancy=occupancy/dble(event_max)
    tmp2=tmp2/dble(event_max)
    chi0=sum(tmp2)
    chi2pi=0.d0
    do i=0,nx-1
      do j=0,ny-1
        do k=0,nz-1
          chi2pi=chi2pi+tmp2(i,j,k)*cdexp(Im1*(2.d0*pi/(nx*a))*i)
        end do
      end do
    end do

    susceptibility=sum(susceptibility_per_event(1,:))/sum(susceptibility_per_event(2,:))
    correlation_length=merge(dsqrt(susceptibility-1.d0),dsqrt(1.d0-susceptibility),susceptibility>1.d0)
    r1=sum(susceptibility_per_event(1,:))/sum(susceptibility_per_event(3,:))
    r2=merge(dsqrt(r1-1.d0),dsqrt(1.d0-r1),r1>1.d0)
    if (r2>correlation_length) correlation_length=r2 !max(x,y)

    deallocate(charge_i,charge_j,charge_k,charge_mask,e_coul_tmp,ind,e_total_per_event, &
      m_order_per_event,rate_cumulative,rate_cumulative2,tmp1,tmp2,susceptibility_per_event, &
      correlation_length_per_event)
    info=0
  end subroutine COMPUTE_REPLICA

  subroutine ORDER_CORRELATION_1D(n,charge,jk,m_order,correlation_length)
    integer,intent(in) :: n,charge(n),jk
    real(8),intent(out) :: m_order,correlation_length
    integer i,i0,i1
    real(8) r1
    complex(8) :: chi0,chi2pi

    r1=0.d0;m_order=0.d0
    do i=1,n
      r1=r1+merge(1.d0,-1.d0,charge(i)>0)*(-1)**(i+jk)
    end do !i
    m_order=m_order+dabs(r1)

    !finite-size (charge order) correlation length, or two-point correlation function
    chi0=0.d0;chi2pi=0.d0
    do i=1,nx
      do i0=i,i+nx-1
        i1=MD(i0,nx)
        r1=merge(1.d0,-1.d0,charge(i)>0)*(-1)**(i+jk)*merge(1.d0,-1.d0,charge(i1)>0)*(-1)**(i1+jk)
        chi0=chi0+r1 !susceptibility
        chi2pi=chi2pi+r1*cdexp(Im1*(2.d0*pi/((nx-1)*a))*(i0-i)) !fourier transform at momentum 2pi/L
      end do !i0
    end do !i
    chi0=chi0/dble(nx*(nx-1))
    chi2pi=chi2pi/dble(nx*(nx-1))
    r1=chi0/chi2pi
    correlation_length=merge(dsqrt(r1-1.d0),dsqrt(1.d0-r1),r1>1.d0)
  end subroutine ORDER_CORRELATION_1D

  !Driscoll_PhD(22) Appendix B
  subroutine COMPUTE_EWALD(nx,ny,nz,chargee,e_coule)
    integer,intent(in) :: nx,ny,nz,chargee(nx,ny,nz)
    real(8),intent(out) :: e_coule(nx,ny,nz)
    real(8) r1,r2,e_ke(nx,ny,nz),e_0e(nx,ny,nz),km,charge1,kmx,kmy,kmz
    integer :: i,j,k,i0,j0,k0,i1,j1,k1,m,mx,my,mz

    e_coule=0.d0;e_ke=0.d0
    r2=(2.d0*cutoff)**2

    do i=1,nx
      do j=1,ny
        do k=1,nz
          do i0=i-r_cut_c_x,i+r_cut_c_x
            do j0=j-r_cut_c_y,j+r_cut_c_y
              do k0=k-r_cut_c_z,k+r_cut_c_z
                i1=MD3(i0,nx)
                j1=MD3(j0,ny)
                k1=MD3(k0,nz)
                charge1=merge(0.5d0,-0.5d0,chargee(i1,j1,k1)>0)
                !direct Coulomb term gets suppressed very strongly, by 10^{-15} over few (3-4) sites for cutoff 2pi/L
                if ((i0-i)**2+(j0-j)**2+(k0-k)**2<=r_cut_c_y**2) then !sphere cut off
                  r1=DISTANCE3(i,j,k,i0,j0,k0,i1,j1,k1) ![a]
                  if (i0/=i.or.j0/=j.or.k0/=k) then
                    !UGAMMA(0.5,x)=sqrt(pi)*erfc(sqrt(x))
                    e_coule(i,j,k)=e_coule(i,j,k)+charge1*erfc(cutoff*r1)/r1 !UGAMMA(0.5d0,(cutoff*r1)**2)
                  end if
                end if
                !reciprocal term gives major contribution
                if (abs(i0-i)<nx.and.abs(j0-j)<ny.and.abs(k0-k)<nz) then ![0,ny). r_cut_c_y = ny-1 minimum
                  do mx=-n_modes,n_modes
                    kmx=mx*2.d0*pi/dble(nx) !cutoff
                    do my=-n_modes,n_modes
                      kmy=my*2.d0*pi/dble(ny) !cutoff
                      do mz=-n_modes,n_modes
                        kmz=mz*2.d0*pi/dble(nz) !cutoff
                        if (kmx==0.and.kmy==0.and.kmz==0) cycle
                        km=kmx**2+kmy**2+kmz**2 !km^2
                        e_ke(i,j,k)=e_ke(i,j,k)+charge1*dexp(-km/r2)*cos(kmx*(i0-i)+kmy*(j0-j)+kmz*(k0-k))/km
                      end do
                    end do
                  end do
                end if
              end do !k0
            end do !j0
          end do !i0
          e_0e(i,j,k)=merge(0.5d0,-0.5d0,chargee(i,j,k)>0)*cutoff
        end do !k
      end do !j
    end do !i
    !Eq. (31) in https://juser.fz-juelich.de/record/16155/files/IAS_Series_06.pdf
    e_ke=e_ke*2.d0*pi/(nx*ny*nz) !*k_coul/(pi*nx*ny*nz) !

    e_coule=e_coule+e_ke-e_0e
  end subroutine COMPUTE_EWALD

  real(8) function COMPUTE_EWALD_MADELUNG()
    real(8) r1,r2,e_coulm,e_km,e_0m,km,charge1,kmx,kmy,kmz
    integer :: i,j,k,i0,j0,k0,i1,j1,k1,m,mx,my,mz

    e_coulm=0.d0;e_km=0.d0
    i=nx/2;j=ny/2;k=nz/2-1
    r2=(2.d0*cutoff)**2

    !direct term
    do i0=i-r_cut_c_x,i+r_cut_c_x
      do j0=j-r_cut_c_y,j+r_cut_c_y
        do k0=k-r_cut_c_z,k+r_cut_c_z
          charge1=(-1)**(i0+j0+k0)
          if ((i0-i)**2+(j0-j)**2+(k0-k)**2<=r_cut_c_y**2) then !sphere cut off
            i1=MD3(i0,nx)
            j1=MD3(j0,ny)
            k1=MD3(k0,nz)
            r1=DISTANCE3(i,j,k,i0,j0,k0,i1,j1,k1) ![a]
            if (i0/=i.or.j0/=j.or.k0/=k) then
              !UGAMMA(0.5,x)=sqrt(pi)*erfc(sqrt(x))
              e_coulm=e_coulm+charge1*erfc(cutoff*r1)/r1 !UGAMMA(0.5d0,(cutoff*r1)**2)
            end if
          end if
          do mx=-n_modes,n_modes
            kmx=mx*2.d0*pi/dble(nx) !cutoff
            do my=-n_modes,n_modes
              kmy=my*2.d0*pi/dble(ny) !cutoff
              do mz=-n_modes,n_modes
                kmz=mz*2.d0*pi/dble(nz) !cutoff
                if (kmx==0.and.kmy==0.and.kmz==0) cycle
                km=kmx**2+kmy**2+kmz**2 !km^2
                e_km=e_km+charge1*dexp(-km/r2)*cos(kmx*(i0-i)+kmy*(j0-j)+kmz*(k0-k))/km
              end do
            end do
          end do
        end do !k0
      end do !j0
    end do !i0
    e_0m=(-1)**(i+j+k)*cutoff

    !Eq. (31) in https://juser.fz-juelich.de/record/16155/files/IAS_Series_06.pdf
    e_km=e_km*2.d0*pi/(nx*ny*nz) !/(pi*nx*ny*nz) !
    COMPUTE_EWALD_MADELUNG=e_coulm+e_km-e_0m
  end function COMPUTE_EWALD_MADELUNG

  real(8) function COMPUTE_EWALD_PARTIAL(i,j,k,i0,j0,k0,i1,j1,k1,charge2)
    integer,intent(in) :: i,j,k,i0,j0,k0,i1,j1,k1,charge2
    real(8) r1,r2,e_coulp,e_kp,km,kmx,kmy,kmz,charge1
    integer :: m,mx,my,mz

    e_coulp=0.d0;e_kp=0.d0
    r2=(2.d0*cutoff)**2

    charge1=merge(0.5d0,-0.5d0,charge2>0)
    if ((i-i0)**2+(j-j0)**2+(k-k0)**2<=r_cut_c_y**2) then !sphere cut off
      r1=DISTANCE3(i,j,k,i0,j0,k0,i1,j1,k1) ![a]
      if (i0/=i.or.j0/=j.or.k0/=k) then
        !UGAMMA(0.5,x)=sqrt(pi)*erfc(sqrt(x))
        e_coulp=charge1*erfc(cutoff*r1)/r1 !UGAMMA(0.5d0,(cutoff*r1)**2)
      end if
    end if
!    if ((i0-i)**2+(j0-j)**2+(k0-k)**2<=r_cut_f**2) then
!      do m=-n_modes,n_modes
!        if (m==0) cycle
!        km=m*cutoff
!        !UGAMMA(1,x)=exp(-x)
!!        e_k=e_k+charge1*dexp(-(km/(2.d0*cutoff))**2)*cos(km*r1)/km**2 !UGAMMA(1.0d0,(km/(2.d0*cutoff))**2)
!        e_k=e_k+charge1*dexp(-(km/(2.d0*cutoff))**2)* &
!           cos(km*(j0-j)+km*(i0-i)+km*(k0-k))/km**2 !r1!UGAMMA(1.0d0,(km/(2.d0*cutoff))**2)!
!      end do
!    end if
!    e_k=e_k*2.d0*pi/(nx*ny*nz)
    if (abs(i0-i)<nx.and.abs(j0-j)<ny.and.abs(k0-k)<nz) then
      do mx=-n_modes,n_modes
        kmx=mx*2.d0*pi/dble(nx) !cutoff
        do my=-n_modes,n_modes
          kmy=my*2.d0*pi/dble(ny) !cutoff
          do mz=-n_modes,n_modes
            kmz=mz*2.d0*pi/dble(nz) !cutoff
            if (kmx==0.and.kmy==0.and.kmz==0) cycle
            km=kmx**2+kmy**2+kmz**2 !km^2
            e_kp=e_kp+charge1*dexp(-km/r2)*cos(kmx*(i0-i)+kmy*(j0-j)+kmz*(k0-k))/km
          end do
        end do
      end do
    end if
    e_kp=e_kp*2.d0*pi/(nx*ny*nz) !*k_coul/(pi*nx*ny*nz)

!    e_0=0.d0 !merge(0.5d0,-0.5d0,charge(i,j,k)>0)*cutoff
 
    COMPUTE_EWALD_PARTIAL=e_coulp+e_kp !-e_0
  end function COMPUTE_EWALD_PARTIAL


end module coulomb
