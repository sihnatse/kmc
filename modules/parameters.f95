module parameters
  implicit none

  integer :: myid=0
  complex(8) :: Im1=(0.d0,1.d0)
  real(8) :: pi=3.141592653589793238d0,me=9.1d-31,hpl=1.055d-34,qe=1.6d-19,eps0=8.85e-12,Ry=2.17991d-18,kB=1.381d-23

  integer :: nx,ny,nz,n_charge,n_event,version,n_c_period
  real(8) :: a,bias,bias2,loc_length,loc_length2,dx,dy,dz,dx2,dy2,dz2, &
             k_x,k_y,k_z,coldness,e_dis_strength
  integer, allocatable :: sc_id(:,:,:)
  !pos(?,x,y,z) in units of a
  real(8), allocatable :: energy(:,:,:),e_coul(:,:,:),pos(:,:,:,:)
  character(32) :: dos,initc,output_file='output.dat'
  logical :: eliminate_soft_clusters=.false.,verbose=.false.,pos_disorder=.false.

  !Coulomb global parameters
  real(8) :: ewald_cutoff
  logical :: coulomb=.false.,ground_state=.false.,replica_exchange=.false.,ewald=.false.

  !active global parameters
  real(8) :: speed_global
  real(8), allocatable :: speed(:,:)

  type ptr
    integer, dimension(:,:), pointer :: event
  end type ptr
  type(ptr), dimension(:), allocatable :: track

  type ptr2
    integer, dimension(:,:), allocatable :: event
    integer :: size=0
  end type ptr2
  type(ptr2), dimension(:), allocatable :: sc

  type point
    real(8) :: temperature,bias,e_dis_strength
    integer :: r_cut
  end type
  type(point), dimension(:), allocatable :: calc
  integer :: calc_size=0

contains

  integer function MD(a,p)
    integer,intent(in) :: a,p

    if (a>p) then
      MD=a-p
    else if (a<1) then
      MD=p+a
    else
      MD=a
    end if
  end function MD

  integer function MD2(a,p,b)
    integer,intent(in) :: a,p,b

    if (a>p) then
      MD2=a-b
    else if (a<-p) then
      MD2=a+b
    else
      MD2=a
    end if
  end function MD2

  integer function MD3(a,p)
    integer,intent(in) :: a,p

    MD3=mod(a,p)
    if (MD3==0) then
      MD3=p
    else if (MD3<0) then
      MD3=MD3+p
    end if
  end function MD3

  !i1,j1,k1 in +/- domain range
  real(8) function DISTANCE(i0,j0,k0,i1,j1,k1,i2,j2,k2)
    integer,intent(in) :: i0,j0,k0,i1,j1,k1,i2,j2,k2
    real(8) :: dx,dy,dz,r(3)

    dx=0.d0;dy=0.d0;dz=0.d0 !must be initialized like this
    if (i1>i2) then
      dx=dx2
    else if (i1<i2) then
      dx=-dx2
    end if
    if (j1>j2) then
      dy=dy2
    else if (j1<j2) then
      dy=-dy2
    end if
    if (k1>k2) then
      dz=dz2
    else if (k1<k2) then
      dz=-dz2
    end if

    r=pos(:,i2,j2,k2)-pos(:,i0,j0,k0)+(/dx,dy,dz/)
    DISTANCE=dsqrt(r(1)**2+r(2)**2+r(3)**2)
  end function DISTANCE

  !i1,j1,k1 can be any integer [-infty,+infty]
  real(8) function DISTANCE3(i0,j0,k0,i1,j1,k1,i2,j2,k2)
    integer,intent(in) :: i0,j0,k0,i1,j1,k1,i2,j2,k2
    real(8) :: dx,dy,dz,r(3)

    dx=dx2*int((i1-i2)/nx)
    dy=dy2*int((j1-j2)/nx)
    dz=dz2*int((k1-k2)/nz)
    r=pos(:,i2,j2,k2)-pos(:,i0,j0,k0)+(/dx,dy,dz/)
    DISTANCE3=dsqrt(r(1)**2+r(2)**2+r(3)**2)
  end function DISTANCE3

  subroutine DUMP_PATH(n,increment,dumped)
    integer,intent(in) :: n
    integer,intent(inout) :: dumped
    logical,intent(in) :: increment
    integer :: j,sa,i4,j4,k4
    character(8) :: fsuf='00000000'

    open(4,file='track.dat',status='old',access='append')
    j=1; sa=size(track(n)%event,dim=2) !j=2
    do while (j<=sa.and.track(n)%event(1,j)>0)
!      if (j>1.and.abs(i4-track(n)%event(j,1))>nx/2) write (4,'(3(2x,"-"))')
      i4=track(n)%event(1,j); j4=track(n)%event(2,j); k4=track(n)%event(3,j)
      write (4,'(3(2x,i4))') i4,j4,k4
      j=j+1
    end do
    if (increment.and.j>2) dumped=dumped+1
    write (4,'(3(2x,i4))') 0,0,0
    close (4)
  end subroutine DUMP_PATH

  !https://en.wikipedia.org/wiki/Incomplete_gamma_function#
  real(8) function UGAMMA(s,x)
    real(8), intent(in) :: s,x
    integer k

    UGAMMA=0.d0
    do k=0,100
      UGAMMA=UGAMMA+(-1)**k*(x)**(s+k)/(fact(k)*(s+k))
    end do !k
    UGAMMA=gamma(s)-UGAMMA
    if (UGAMMA<0.d0) UGAMMA=0.d0 !print *,"Warning: upper gamma function is negative! Increase sum index."
  end function

  real(8) function fact(n)
    integer, intent(in) :: n
    integer :: i

    if (n<0) error stop 'factorial is singular for negative integers'
    fact=1.0
    do i=2,n
      fact=fact*i
    end do
  end function fact

end module parameters
