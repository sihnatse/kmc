#!/bin/bash

COMMON_ARGS="\
--nx 20 --ny 20 --nz 20 \
--loc_length 1.0 \
--dos gaussian \
--events 1 --charges 715 \
--verbose \
--soft-clusters "
#--events 1000 --charges 32 \ 100000000

#--calc <temperature> <E disorder strength> <bias> <cut-off radius>
# parameters for Mott law test
CALCULATION_ARGS="\
--calc 0.25 0.0 0.0 6 "
#--calc 0.2 0.0 0.2 6 "0.25
#--calc 0.05 0.0 0.25 9 "
#--calc 1.0 0.0 5.0 6 \
#--calc 0.5 0.0 2.5 6 \
#--calc 0.2 0.0 1.0 7 \
#--calc 0.4 0.0 1.0 3 \
#--calc 0.3 0.0 1.0 3 \
#--calc 0.27 0.0 1.0 3 \
#--calc 0.25 0.0 1.0 3 \
#--calc 0.22 0.0 1.0 3 \
#--calc 0.2 0.0 1.0 3 \
#--calc 0.15 0.0 1.0 3 \
#--calc 0.1 0.0 1.0 3 "
#--calc 1.0 0.0 1.0 3 \
#--calc 0.8 0.0 1.0 3 \
#--calc 0.7 0.0 1.0 3 \
#--calc 0.6 0.0 1.0 3 \
#--calc 1.2 0.0 1.0 5 \
#--calc 1.5 0.0 1.0 5 "
#--calc 0.15 0.0 1.0 5 \
#--calc 0.2 0.0 1.0 5 \
#--calc 0.25 0.0 1.0 5 \
#--calc 0.3 0.0 1.0 5 \
#--calc 0.4 0.0 1.0 5 \
#--calc 0.5 0.0 1.0 5 \

# output file must exist before writing to it
if [ ! -f output.dat ]; then
  touch output.dat
fi

if [ ! -f dos.dat ]; then
  touch dos.dat
fi

# write header with names of computed parameters
if [ ! -s output.dat ]; then
  echo "  temperature         bias  conductivty  hopdistance" \
       "  etransport  rcuthit  waitingtime  dumpd      events" >> output.dat
fi

for ((i = 0; i < 100; i++))
do
./build/kmc \
  $COMMON_ARGS $CALCULATION_ARGS
done

